#include "TriMeshSimplification.h"
#include "MeshConverter.h"

using MedBox::Modeling::TriMeshSimplification;
using MedBox::Modeling::MeshConverter;

TriEdgeCollapseQuadricParameter TriMeshSimplification::parameters;

const void TriMeshSimplification::Simplify(M::TriMesh* mesh                   ,
                                           const float target_error           ,
                                           float reduce_fraction              ,
                                           const bool cleaning_flag)
{
    TriEdgeCollapseQuadricParameter parameters;

    //Start decimation
    if (reduce_fraction > 1)
        reduce_fraction = 1.0f;

    if (reduce_fraction < 0) { cout << "Ratio must be between zero and one.\n"; return; }

    //Calculate final size mesh
    int current_size = mesh->GetFaceCount();
    int final_size = static_cast<int>(current_size * reduce_fraction);

    VCGMesh vcg_mesh;
    MeshConverter<TriMesh>::ConvertToVCGTriMesh(mesh, vcg_mesh);

    if (cleaning_flag)
    {
        int dup = tri::Clean<VCGMesh>::RemoveDuplicateVertex(vcg_mesh);
        int unref = tri::Clean<VCGMesh>::RemoveUnreferencedVertex(vcg_mesh);
        cout << "Removed " << dup << "duplicate and " << unref << "unreferenced vertices from mesh \n";
    }

    cout << "reducing it to " << final_size << "\n";

    UpdateBounding<VCGMesh>::Box(vcg_mesh);

    // decimator initialization
    LocalOptimization<VCGMesh> DeciSession(vcg_mesh, &parameters);

    DeciSession.Init<VCGTriEdgeCollapse>();

    cout << "Initial heap size " << int(DeciSession.h.size()) << "\n";

    DeciSession.SetTargetSimplices(final_size);
    DeciSession.SetTimeBudget(0.5f);
    DeciSession.SetTargetOperations(100000);
    if (target_error < (std::numeric_limits<float>::max)())
        DeciSession.SetTargetMetric(target_error);

    while (DeciSession.DoOptimization() && vcg_mesh.fn > final_size && DeciSession.currMetric < target_error)
        printf("Current Mesh size %7i heap sz %9i err %9g \n", vcg_mesh.fn, int(DeciSession.h.size()), DeciSession.currMetric);

    printf("Mesh  %d %d error %g \n", vcg_mesh.vn, vcg_mesh.fn, DeciSession.currMetric);

    Allocator<VCGMesh>::CompactFaceVector(vcg_mesh);
    Allocator<VCGMesh>::CompactVertexVector(vcg_mesh);

    MeshConverter<TriMesh>::ConvertToTriMesh(vcg_mesh, mesh);
    vcg_mesh.Clear();
}

const void TriMeshSimplification::SetBoundaryWeight(bool value)
{
    parameters.BoundaryWeight = value;
}

const void TriMeshSimplification::SetCosineThr(Float64 value)
{
    parameters.CosineThr = value;
}

const void TriMeshSimplification::SetFastPreserveBoundary(bool value)
{
    parameters.FastPreserveBoundary = value;
}

const void TriMeshSimplification::SetNormalCheck(bool value)
{
    parameters.NormalCheck = value;
}

const void TriMeshSimplification::SetOptimalPlacement(bool value)
{
    parameters.OptimalPlacement = value;
}

const void TriMeshSimplification::SetPreserveBoundary(bool value)
{
    parameters.PreserveBoundary = value;
}

const void TriMeshSimplification::SetPreserveTopology(bool value)
{
    parameters.PreserveTopology = value;
}

const void TriMeshSimplification::SetQualityCheck(bool value)
{
    parameters.QualityCheck = value;
}

const void TriMeshSimplification::SetQualityQuadric(bool value)
{
    parameters.QualityQuadric = value;
}

const void TriMeshSimplification::SetQualityWeight(bool value)
{
    parameters.QualityWeight = value;
}

const void TriMeshSimplification::SetQualityWeightFactor(Float64 value)
{
    parameters.QualityWeightFactor = value;
}

const void TriMeshSimplification::SetScaleFactor(Float64 value)
{
    parameters.ScaleFactor = value;
}

const void TriMeshSimplification::SetScaleIndependent(bool value)
{
    parameters.ScaleIndependent = value;
}

const void TriMeshSimplification::SetUseArea(bool value)
{
    parameters.UseArea = value;
}

const void TriMeshSimplification::SetUseVertexWeight(bool value)
{
    parameters.UseVertexWeight = value;
}

const void TriMeshSimplification::SetQualityThr(Float64 value)
{
    parameters.QualityThr = value;
}

const void TriMeshSimplification::SetQuadricEpsilon(Float64 value)
{
    parameters.QuadricEpsilon = value;
}

const void TriMeshSimplification::SetNormalThrRad(Float64 value)
{
    parameters.NormalThrRad = value;
}
