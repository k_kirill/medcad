#include "main.h"

#include "TriMeshSmoothing.h"

using MedBox::Utils::Array1D_Allocator;
using MedBox::Modeling::TriMeshSmoothing;
using Vert = MedBox::Modeling::Geometry::Vertex;

const void TriMeshSmoothing::TaubinSmooth(TriMesh* mesh, const Float32 lambda, const Float32 mu, const Uint16 iterations)
{
	cout << "TriMeshSmoothing use Taubin lambda|mu algorithm ";
	cout << "(inverse neighbour count weighting)" << "\n";

	for (unsigned short i = 0; i < iterations; ++i)
	{
		cout << "Iteration " << i + 1 << " of " << iterations << "\n";

		laplaceSmooth(mesh, lambda);
		laplaceSmooth(mesh, mu);
	}
	mesh->GenerateTriangleNormals();
}

const void TriMeshSmoothing::laplaceSmooth(TriMesh* mesh, const float scale)
{
	if (mesh->GetFaceCount() != 0)
	{
		vector<Vert> vertices = mesh->GetVertices();
		vector<vector<Uint32>> vertex_to_vertex_indices = mesh->GetVertexListToVertexIndices();

        Array1D_Allocator<Vert> displacements_arr(vertices.size());

		// Get per-vertex displacement
        Vert* displacements = displacements_arr.Get();
		for (size_t i = 0; i < vertices.size(); i++)
		{
			// Skip rogue vertices (which were probably made rogue during a previous
			// attempt to fix mesh cracks).
			if (0 == vertex_to_vertex_indices[i].size())
				continue;

			const float weight = 1.0f / static_cast<float>(vertex_to_vertex_indices[i].size());

			for (size_t j = 0; j < vertex_to_vertex_indices[i].size(); j++)
			{
				size_t neighbour_j = vertex_to_vertex_indices[i][j];
				displacements[i] += (vertices[neighbour_j] - vertices[i])*weight;
			}
		}

		for (unsigned int i = 0; i < vertices.size(); i++)
			vertices[i] += (displacements[i] * scale);
	}

	else
	{
		cout << "'MarchingCube' class object is empty!\n";
	}
}