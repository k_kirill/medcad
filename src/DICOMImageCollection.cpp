#include "DICOMImageCollection.h"

using MedBox::Imaging::DICOMImageCollection;
using MedBox::Imaging::DICOMImage;

DICOMImageCollection::DICOMImageCollection(DICOMImage* images) : _images(images)
{
}

DICOMImageCollection::DICOMImageCollection(Uint32 size, const Uint16 offset)
{
    _size = size;
    _offset = offset;
    _images = new DICOMImage[size];
}

DICOMImageCollection::~DICOMImageCollection()
{
    delete[] _images;
}

const void DICOMImageCollection::Add(const DICOMImage& image)
{
    if (!IsFull())
    {
        _images[_count] = image;
        _count++;
    }
}

vector<DICOMImageCollection*> DICOMImageCollection::Split(DICOMImageCollection* collection, Uint32 count)
{
    vector<DICOMImageCollection*> collections;
    Uint32 img_count = collection->GetCount();
    Uint32 piece = static_cast<Uint32>(img_count / count);

    for (size_t i = 0; i < count; ++i)
    {
        if (i == count - 1)
            collections.push_back(new DICOMImageCollection(img_count - (i * piece), i * piece));

        else
            collections.push_back(new DICOMImageCollection(piece, i * piece));
    }

    const DICOMImage* images = collection->GetImages();
    int k = 0;
    for (size_t i = 0; i < count; ++i)
    {
        Uint32 size = collections[i]->GetSize();
        for (size_t j = 0; j < size; ++j, ++k)
        {
            collections[i]->Add(images[k]);
        }
    }

    delete collection;
    return collections;
}

DICOMImageCollection* DICOMImageCollection::Merge(const vector<DICOMImageCollection*>& collections)
{
    Uint32 sum_count = 0;
    for (const DICOMImageCollection* collection : collections)
        sum_count += collection->GetCount();

    DICOMImageCollection* collection = new DICOMImageCollection(sum_count);

    for (const DICOMImageCollection* coll : collections)
    {
        const DICOMImage* images = coll->GetImages();
        for (size_t i = 0; i < coll->GetCount(); ++i)
        {
            collection->Add(images[i]);
        }
    }

    for (const DICOMImageCollection* collection : collections)
        delete collection;

    return collection;
}

OBJECT_TYPE DICOMImageCollection::GetType() const
{
    return _type;
}

const Uint32 DICOMImageCollection::GetCount() const
{
	return _count;
}

const Uint32 DICOMImageCollection::GetSize() const
{
    return _size;
}

const Uint16 MedBox::Imaging::DICOMImageCollection::GetOffset() const
{
    return _offset;
}

const DICOMImage* DICOMImageCollection::GetImages() const
{
	return _images;
}

const DICOMImage& DICOMImageCollection::First() const
{
    return _images[0];
}

const DICOMImage& DICOMImageCollection::Last() const
{
    return _images[_count - 1];
}

const DICOMImage& DICOMImageCollection::Image(Uint32 index) const
{
    if (index < _count && index >= 0)
        return _images[index];

    throw std::exception("index out of range!");
}

bool DICOMImageCollection::IsEmpty() const
{
    if (_count == 0)
        return true;

    return false;
}

bool DICOMImageCollection::IsFull() const
{
    if (_size == _count)
        return true;

    return false;
}
