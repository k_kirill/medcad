#include "TriMeshRepairing.h"
#include "main.h"
#include "Exporter.h"
#include "MeshConverter.h"

using MedBox::Modeling::TriMeshRepairing;
using MedBox::Modeling::MeshConverter;

using namespace VCGTypes;
using Tri = MedBox::Modeling::Geometry::Triangle;

const void TriMeshRepairing::RepairAll(M::TriMesh* mesh)
{
	RemoveSharedTrianglesByEdge(mesh);
	RemoveTrianglesFromFans(mesh);
	RemoveSelfIntersection(mesh);
}

const void TriMeshRepairing::RemoveTrianglesFromFans(M::TriMesh* mesh)
{
	//Remove excess triangle from triangle fan
	set<Uint32, std::greater<Uint32>> removable_triangle_indices;

	vector<Tri> triangles = mesh->GetTriangleList();
	vector<vector<Uint32>> triangle_to_edge_indices = mesh->GetTriangleListToEdgeIndices();
	vector<vector<Uint32>> edge_to_triangle_indices = mesh->GetEdgeListToTriangleIndices();

	if (triangles.size() == 0) { cout << "Error: model is empty!\n"; return; }
	if (!NonManifoldVerticesIsExist(mesh)) { cout << "Every vertex is manifold!\n"; return; }
	cout << "Erasing non-manifold vertices...\n";

	int iter = 1;
	do 
	{
		removable_triangle_indices.clear();

		for (Uint32 i = 0; i < triangles.size(); ++i)
		{
			Uint32 edge_1 = triangle_to_edge_indices[i][0];
			Uint32 edge_2 = triangle_to_edge_indices[i][1];
			Uint32 edge_3 = triangle_to_edge_indices[i][2];

			if (edge_to_triangle_indices[edge_1].size() == 1 && 
				edge_to_triangle_indices[edge_2].size() == 1 &&
				edge_to_triangle_indices[edge_3].size() == 1)
				removable_triangle_indices.insert(i);
		}

		if (removable_triangle_indices.size() != 0)
		{
			/*list<Tri> trs;

			for (set<unsigned int>::iterator it = removable_triangle_indices.begin();
											 it != removable_triangle_indices.end(); ++it)
				trs.push_back(triangles[*it]);

			M::TriMesh m(trs);
			string fileName = "D:/Study/Term_project/Models/crack2.stl";
			Service::IO::Exporter<TriMesh>::exportToSTL(&m, fileName.c_str(), false);*/

			//Remove founded triangles
			for (auto it = removable_triangle_indices.begin(); it != removable_triangle_indices.end(); ++it)
                triangles.erase(triangles.begin() + (*it));

			std::cout << "Iteration " << iter << ": "
					  << "Excess triangles was deleted: " 
					  << removable_triangle_indices.size() << " from triangle fans" << "\n";
			//---------------------------------------------------------------------------------------------------------

			mesh->Update(); iter++;
		}
	} while (removable_triangle_indices.size() != 0);

	cout << "Erasing was done!\n";
	mesh->CalculateQuality();
}

const void TriMeshRepairing::RemoveSharedTrianglesByEdge(M::TriMesh* mesh)
{
	// Find edges that belong more once triangle.
	set<Uint32, greater<Uint32>> removable_triangle_indices;
	list<Uint32> indices;

	vector<vector<Uint32>> edge_to_triangle_indices = mesh->GetEdgeListToTriangleIndices();
	vector<Tri> triangles = mesh->GetTriangleList();

	if (triangles.size() == 0) { cout << "Error: model is empty!\n"; return; }
	if (!NonManifoldEdgesIsExist(mesh)) { cout << "Every edge is manifold!\n"; return; }
	cout << "Erasing non-manifold edges...\n";

	int iter = 1;
	do
	{
		removable_triangle_indices.clear();

		for (Uint32 i = 0; i < edge_to_triangle_indices.size(); ++i)
		{
			if (edge_to_triangle_indices[i].size() > 2)
			{
				removable_triangle_indices.insert(edge_to_triangle_indices[i].begin(),
												  edge_to_triangle_indices[i].end());
			}
		}

		//Delete triangles sharing an edge with at least two other triangles
		if (removable_triangle_indices.size() != 0)
		{
			/*list<Tri> trs;

			for (set<unsigned int>::iterator it = removable_triangle_indices.begin(); 
											 it != removable_triangle_indices.end(); ++it)
				trs.push_back(triangles[*it]);

			M::TriMesh m(trs);
			string fileName = "D:/Study/Term_project/Models/crack1.stl";
			Service::IO::Exporter<TriMesh>::exportToSTL(&m, fileName.c_str(), false);*/

			//Remove founded triangles
			for (auto it = removable_triangle_indices.begin(); it != removable_triangle_indices.end(); ++it)
				triangles.erase(triangles.begin() + (*it));

			cout << "Iteration " << iter << ": "
				 << "Triangles shared an edge with at least two other triangles was deleted: "
				 << removable_triangle_indices.size() << "\n";
			//---------------------------------------------------------------------------------------------------------

			mesh->Update(); 
            iter++;
		}
	} while (removable_triangle_indices.size() != 0);

	cout << "Erasing was done!\n";
	mesh->CalculateQuality();
}

const void TriMeshRepairing::RemoveSelfIntersection(M::TriMesh* mesh)
{
    VCGTypes::VCGMesh vcg_mesh;
    vector<VCGTypes::VCGFace*> bad_faces;

	if (SelfIntersectionIsExist(mesh, vcg_mesh, bad_faces))
	{
		//list<Tri> trs;
		for (int i = 0; i < bad_faces.size(); ++i)
		{
			/*Vert v[3];
			VCGMesh::CoordType coord0 = bad_faces[i]->P(0);
			VCGMesh::CoordType coord1 = bad_faces[i]->P(1);
			VCGMesh::CoordType coord2 = bad_faces[i]->P(2);

			v[0] = Vert(coord0.X(), coord0.Y(), coord0.Z());
			v[1] = Vert(coord1.X(), coord1.Y(), coord1.Z());
			v[2] = Vert(coord2.X(), coord2.Y(), coord2.Z());

			Tri t(v);
			trs.push_back(t);*/

			tri::Allocator<VCGMesh>::DeleteFace(vcg_mesh, *bad_faces[i]);
		}
		/*shared_ptr<M::TriMesh> tr_m(new M::TriMesh(trs));
		Service::IO::Exporter<M::TriMesh>::exportToPLY(tr_m, "D:/Study/Term_project/Models/crack.ply", false);*/
		Allocator<VCGMesh>::CompactFaceVector(vcg_mesh);
		Allocator<VCGMesh>::CompactVertexVector(vcg_mesh);

		MeshConverter<M::TriMesh>::ConvertToTriMesh(vcg_mesh, mesh);
	}
}

const bool TriMeshRepairing::NonManifoldEdgesIsExist(M::TriMesh* mesh)
{
	// Find edges that belong more once triangle.
	set<Uint32, greater<Uint32>> removable_triangle_indices;

	vector<vector<Uint32>> edge_to_triangle_indices = mesh->GetEdgeListToTriangleIndices();
	vector<Tri> triangles = mesh->GetTriangleList();

	if (triangles.size() == 0) { cout << "Error: model is empty!\n"; return false; }

	removable_triangle_indices.clear();

	for (Uint32 i = 0; i < edge_to_triangle_indices.size(); ++i)
	{
		if (edge_to_triangle_indices[i].size() > 2)
			return true;
	}

	return false;
}

const bool TriMeshRepairing::NonManifoldVerticesIsExist(M::TriMesh* mesh)
{
	//Remove excess triangle from triangle fan
	set<unsigned int, std::greater<unsigned int>> removable_triangle_indices;

	vector<Tri> triangles = mesh->GetTriangleList();
	vector<vector<Uint32>> triangle_to_edge_indices = mesh->GetTriangleListToEdgeIndices();
	vector<vector<Uint32>> edge_to_triangle_indices = mesh->GetEdgeListToTriangleIndices();

	if (triangles.size() == 0) { cout << "Error: model is empty!\n"; return false; }

	for (unsigned int i = 0; i < triangles.size(); ++i)
	{
		Uint32 edge_1 = triangle_to_edge_indices[i][0];
		Uint32 edge_2 = triangle_to_edge_indices[i][1];
		Uint32 edge_3 = triangle_to_edge_indices[i][2];

		if (edge_to_triangle_indices[edge_1].size() == 1 &&
			edge_to_triangle_indices[edge_2].size() == 1 &&
			edge_to_triangle_indices[edge_3].size() == 1)
			return true;
	}

	return false;
}

const bool TriMeshRepairing::SelfIntersectionIsExist(M::TriMesh* mesh, VCGTypes::VCGMesh& vcg_mesh, vector<VCGTypes::VCGFace*>& bad_faces)
{
	MeshConverter<M::TriMesh>::ConvertToVCGTriMesh(mesh, vcg_mesh);

	tri::UpdateNormal<VCGMesh>::PerVertexNormalizedPerFaceNormalized(vcg_mesh);
	tri::UpdateBounding<VCGMesh>::Box(vcg_mesh);
	bool is_self_inter = tri::Clean<VCGMesh>::SelfIntersections(vcg_mesh, bad_faces);

	if (is_self_inter) 
        return true;
    
    return false;
}