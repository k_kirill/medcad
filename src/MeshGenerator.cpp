#include "MeshGenerator.h"

using MedBox::Modeling::GridCell;
using MedBox::Modeling::MeshGenerator;
using MedBox::Modeling::TriMesh;

TriMesh* MeshGenerator::BuildModel(DICOMImageCollection* collection,
                                   const bool standartMC           , 
                                   const GridCell::Size size       , 
                                   const short iso_surface)
{
    cout << "Building model..." << "\n";
    Uint8 cell_size = (Uint8)size;
    list<Tri> T;
    generateTriangles(T, collection, standartMC, size, iso_surface);

    return new TriMesh(T);
}

TriMesh* MeshGenerator::BuildModel(const vector<DICOMImageCollection*>& collections, 
                                   const bool standartMC                           , 
                                   const GridCell::Size size                       ,
                                   const short iso_surface)
{
    cout << "Building model..." << "\n";
    Uint8 cell_size = (Uint8)size;
    Uint16 coll_count = (Uint16)collections.size();

    //Merge adjacent images
    list<Tri> T;
    Uint32 piece = collections[0]->GetCount();
    for (Uint16 i = 0; i < coll_count - 1; ++i)
    {
        list<Tri> trs;
        DICOMImageCollection collection(2, ((i + 1) * piece - 1));
        collection.Add(collections[i]->Last());
        collection.Add(collections[i + 1]->First());

        generateTriangles(trs, &collection, standartMC, cell_size, iso_surface);
        T.splice(T.end(), trs);
    }

    //Build model async
    boost::mutex m;
    vector<boost::thread> workers;

    for (DICOMImageCollection* collection : collections)
    {
        workers.emplace_back(boost::bind<void>([=, &T, &collection, &m]() mutable -> void
        {
            list<Tri> trs(0);
            generateTriangles(trs, collection, standartMC, cell_size, iso_surface);

            if (trs.size() != 0)
            {
                m.lock();
                T.splice(T.end(), trs);
                m.unlock();
            }
        }));
        boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
    }

    std::for_each(workers.begin(), workers.end(), [](boost::thread& worker) -> void
    {
        worker.join();
    });

    return new TriMesh(T);
}

void MeshGenerator::generateTriangles(list<Tri>& triangles, DICOMImageCollection* collection, bool standartMC, Uint8 cell_size, short iso_surface)
{
    const Uint32 count = collection->GetCount();
    const Uint16 rows = collection->Image(0).GetRows();
    const Uint16 columns = collection->Image(0).GetColumns();
    const Uint32 offset = collection->GetOffset();

    Builder* builder;
    switch (collection->SegmentationMark)
    {
    case SEGMENTATION_TYPE::THRESHOLD:
        builder = new Builder(collection, standartMC, cell_size);
        break;

    case SEGMENTATION_TYPE::NONE:
        builder = new Builder(collection, standartMC, cell_size, iso_surface);
        break;

    default:
        builder = new Builder(collection, standartMC, cell_size, iso_surface);
        break;
    }

    for (Uint32 k = 0; k < count - 1; ++k) 
    {
        for (Uint16 j = 0; j < rows - cell_size; j += cell_size) 
        {
            for (Uint16 i = 0; i < columns - cell_size; i += cell_size) 
            {
                if (builder->Build(i, j, k, offset))
                {
                    list<Tri> t = builder->getTriangles();
                    switch (t.size())
                    {
                    case 0:
                        break;

                    default:
                        triangles.splice(triangles.end(), t);
                        break;
                    }
                }
            }
        }
    }
    delete builder;
}
