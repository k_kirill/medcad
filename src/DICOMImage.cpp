#include "DICOMImage.h"

#include <exception>

using MedBox::Imaging::DICOMImage;

DICOMImage::DICOMImage(const char* path)
{
    _path = path;
    parseData();
}

DICOMImage::DICOMImage(const DICOMImage& image)
{
    _rows = image.GetRows();
    _columns = image.GetColumns();
    _patient_name = image.GetPatientName();
    _patient_id = image.GetPatientID();
    _x_pix_sp = image.GetX_PixSP();
    _y_pix_sp = image.GetY_PixSP();
    _slice_loc = image.GetSliceLoc();
    _path = image.GetPath();

    _array = Array2D_Allocator<Sint16>(image.GetArray());
}

DICOMImage::DICOMImage()
{
}

DICOMImage::~DICOMImage()
{
    _array.~Array2D_Allocator();
}

bool DICOMImage::parseData()
{
    DcmFileFormat fileformat;
    OFString p_name;
    OFString p_id;

    try
    {
        OFCondition status = fileformat.loadFile(_path.c_str());
        if (status.good())
        {
            if (fileformat.getDataset()->findAndGetOFString(DCM_PatientID, p_id).good()) { _patient_id = p_id.data(); }

            //Getting patient name
            if (fileformat.getDataset()->findAndGetOFString(DCM_PatientName, p_name).good()) { _patient_name = p_name.data(); }

            //Getting rows data
            if (fileformat.getDataset()->findAndGetUint16(DCM_Rows, _rows).good()) {}

            //Getting columns data
            if (fileformat.getDataset()->findAndGetUint16(DCM_Columns, _columns).good()) {}

            //Getting x pixels spacing
            if (fileformat.getDataset()->findAndGetFloat64(DCM_PixelSpacing, _x_pix_sp).good()) {}

            //Getting y pixels spacing
            if (fileformat.getDataset()->findAndGetFloat64(DCM_PixelSpacing, _y_pix_sp, 1).good()) {}

            //Getting location for the first slice
            if (fileformat.getDataset()->findAndGetFloat64(DCM_SliceLocation, _slice_loc).good()) {}
        }

        else
            cerr << "Error: cannot write DICOM file (" << status.text() << ")" << std::endl;

        DicomImage image(_path.c_str());
        allocate();

        switch (image.getStatus())
        {
        case EIS_Normal:
        {
            extractPixelValues(image);
        } break;

        case EIS_MissingAttribute:
        {
            DJDecoderRegistration::registerCodecs(); // register JPEG codecs
            DcmDataset *dataset = fileformat.getDataset();

            // decompress data set if compressed
            dataset->chooseRepresentation(EXS_LittleEndianExplicit, NULL);

            // check if everything went well
            if (dataset->canWriteXfer(EXS_LittleEndianExplicit))
            {
                fileformat.saveFile(_path.c_str(), EXS_LittleEndianExplicit);

                DicomImage image(_path.c_str());
                extractPixelValues(image);
            }
            DJDecoderRegistration::cleanup(); // deregister JPEG codecs
        } break;

        default:
        {
            cerr << "Cannot detect data representation!" << std::endl;
            return false;
        }
        break;
        }
        fileformat.clear();
        return true;
    }

    catch (...) { return false; }
}

const void DICOMImage::extractPixelValues(const DicomImage& image)
{
    const DiPixel *inter = image.getInterData();

    if (inter != nullptr)
    {
        Sint16 *raw_pixel_data = (Sint16 *)inter->getData();

        if (raw_pixel_data == nullptr) { throw new std::exception("Couldn't acces pixel data!"); }

        _array.CopyData(raw_pixel_data, _columns * _rows);
    }
}

const void DICOMImage::allocate()
{
    _array = Array2D_Allocator<Sint16>(_rows, _columns);
}

const OBJECT_TYPE DICOMImage::GetType() const
{
	return _type;
}

const Uint16 DICOMImage::GetColumns() const
{
	return _columns;
}

const Uint16 DICOMImage::GetRows() const
{
	return _rows;
}

Sint16** DICOMImage::GetData() const
{
	return _array.Get();
}

const string DICOMImage::GetPatientName() const
{
    return _patient_name;
}

const string DICOMImage::GetPatientID() const
{
    return _patient_id;
}

const Float64 DICOMImage::GetX_PixSP() const
{
    return _x_pix_sp;
}

const Float64 DICOMImage::GetY_PixSP() const
{
    return _y_pix_sp;
}

const Float64 DICOMImage::GetSliceLoc() const
{
    return _slice_loc;
}

const Array2D_Allocator<Sint16>& DICOMImage::GetArray() const
{
    return _array;
}

const string DICOMImage::GetPath() const
{
    return _path;
}
