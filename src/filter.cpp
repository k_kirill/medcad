#include "Filter.h"
#include "main.h"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;
using namespace MedBox;
using namespace MedBox::Imaging;

void Filter::GaussianFilter(const DICOMImageCollection* collection, short kernel_size, float sigma)
{
    //Storage convolution matrix
    if (!kernelSizeIsValid(kernel_size))
        throw std::invalid_argument("The kernel size must be odd and positive number!");

    //Get gauss kernel
    Array2D_Allocator<Float32> gk_alloc = getGaussianKernel(kernel_size, sigma);

	//Get extended images
	Array3D_Allocator<Sint16> ext_img_alloc = makeExtendedImages(collection, kernel_size);

	const DICOMImage* images = collection->GetImages();
    const Uint32 count = collection->GetCount();
	const Uint16 rows = images[0].GetRows();
	const Uint16 columns = images[0].GetColumns();

	short sum = 0;

	//Apply Gaussian filter for image
    Float32** gauss_kernel = gk_alloc.Get();
    Sint16*** extended_images = ext_img_alloc.Get();
	for (int m = 0; m < count; ++m)
	{
		for (int k = 0; k < rows; ++k)
		{
			for (int l = 0; l < columns; ++l)
			{
				for (int i = 0; i < kernel_size; ++i)
				{
					for (int j = 0; j < kernel_size; ++j)
					{
						sum += (short)(gauss_kernel[i][j] * extended_images[m][j + k][i + l]);
					}
				}
				images[m].GetData()[k][l] = sum;
				sum = 0;
			}
		}
		cout << "Image " << m << " was handled" << "\n";
	}
}

void Filter::MedianFilter(const DICOMImageCollection* collection, short kernel_size)
{
    Array3D_Allocator<Sint16> ext_img_alloc = makeExtendedImages(collection, kernel_size);

    const DICOMImage* images = collection->GetImages();
    const Uint32 count = collection->GetCount();
    const Uint16 rows = images[0].GetRows();
    const Uint16 columns = images[0].GetColumns();

    //Create a buffer for sorting array
    Sint16 buffer_size = kernel_size * kernel_size;
    Array1D_Allocator<Sint16> buffer_alloc(buffer_size);

    //Sorting array and apply median filter for input image
    Sint16*** extended_images = ext_img_alloc.Get();
    Sint16* buffer = buffer_alloc.Get();
    for (int m = 0; m < count; ++m)
    {
        for (short k = 0; k < rows; ++k)
        {
            for (short l = 0; l < columns; ++l)
            {
                for (short i = 0; i < kernel_size; ++i)
                {
                    for (short j = 0; j < kernel_size; ++j)
                    {
                        buffer[i*kernel_size + j] = extended_images[m][i + k][j + l];
                    }
                }
                images[m].GetData()[k][l] = getMedianValue(buffer, buffer_size, 0, buffer_size);
            }
        }
        cout << "Image " << m << " was handled" << "\n";
    }
}

//This function apply mean filter for input image
void Filter::MeanFilter(const DICOMImageCollection* collection, short kernel_size)
{
    Array3D_Allocator<Sint16> ext_img_alloc = makeExtendedImages(collection, kernel_size);

    const DICOMImage* images = collection->GetImages();
    const Uint32 count = collection->GetCount();
    const Uint16 rows = images[0].GetRows();
    const Uint16 columns = images[0].GetColumns();

    //Initialize matrix element of mean filter
    double matrix_element = 1.0 / (kernel_size*kernel_size);

    //New pixel values will be put here
    short sum = 0;

    //Apply mean filter for image
    Sint16*** extended_images = ext_img_alloc.Get();
    for (int m = 0; m < count; ++m)
    {
        for (short k = 0; k < rows; ++k)
        {
            for (short l = 0; l < columns; ++l)
            {
                for (short i = 0; i < kernel_size; ++i)
                {
                    for (short j = 0; j < kernel_size; ++j)
                    {
                        sum += (short)(matrix_element * extended_images[m][j + k][i + l]);
                    }
                }
                images[m].GetData()[k][l] = sum;
                sum = 0;
            }
        }
        cout << "Image " << m << " was handled" << "\n";
    }
}

void Filter::ErosionFilter(const DICOMImageCollection* collection, short kernel_size)
{
    Array3D_Allocator<Sint16> ext_img_alloc = makeExtendedImages(collection, kernel_size);

    const DICOMImage* images = collection->GetImages();
    const Uint32 count = collection->GetCount();
    const Uint16 rows = images[0].GetRows();
    const Uint16 columns = images[0].GetColumns();

    //To storage pixels area in series
    Sint16 buffer_size = (kernel_size * kernel_size) / 2 + 1;
    Array1D_Allocator<Sint16> buffer_alloc(buffer_size);

    //This algorithm form area of matrix from pixels
    Sint16*** extended_images = ext_img_alloc.Get();
    Sint16* buffer = buffer_alloc.Get();
    for (int m = 0; m < count; ++m)
    {
        for (short k = 0; k < rows; ++k)
        {
            for (short l = 0; l < columns; ++l)
            {
                short index = 0;
                for (short i = 0; i < kernel_size; ++i)
                {
                    if (i <= kernel_size / 2)
                        for (short j = kernel_size / 2 - i; (j >= 0) && (j <= kernel_size / 2 + i); ++j)
                        {
                            buffer[index] = extended_images[m][i + k][j + l];
                            index++;
                        }

                    else
                        for (short j = i - kernel_size / 2; j < kernel_size - (i - kernel_size / 2); ++j)
                        {
                            buffer[index] = extended_images[m][i + k][j + l];
                            index++;
                        }
                }
                images[m].GetData()[k][l] = getMinValue(buffer, buffer_size);
            }
        }
        cout << "Image " << m << " was handled" << "\n";
    }
}

void Filter::DilationFilter(const DICOMImageCollection* collection, short kernel_size)
{
    Array3D_Allocator<Sint16> ext_img_alloc = makeExtendedImages(collection, kernel_size);

    const DICOMImage* images = collection->GetImages();
    const Uint32 count = collection->GetCount();
    const Uint16 rows = images[0].GetRows();
    const Uint16 columns = images[0].GetColumns();

    //To storage pixels area in series
    Sint16 buffer_size = (kernel_size * kernel_size) / 2 + 1;
    Array1D_Allocator<Sint16> buffer_alloc(buffer_size);

    //This algorithm form area of matrix from pixels
    Sint16*** extended_images = ext_img_alloc.Get();
    Sint16* buffer = buffer_alloc.Get();
    for (int m = 0; m < count; ++m)
    {
        for (short k = 0; k < rows; ++k)
        {
            for (short l = 0; l < columns; ++l)
            {
                short index = 0;
                for (short i = 0; i < kernel_size; ++i)
                {
                    if (i <= kernel_size / 2)
                        for (short j = kernel_size / 2 - i; (j >= 0) && (j <= kernel_size / 2 + i); ++j)
                        {
                            buffer[index] = extended_images[m][i + k][j + l];
                            index++;
                        }

                    else
                        for (short j = i - kernel_size / 2; j < kernel_size - (i - kernel_size / 2); ++j)
                        {
                            buffer[index] = extended_images[m][i + k][j + l];
                            index++;
                        }
                }
                images[m].GetData()[k][l] = getMaxValue(buffer, buffer_size);
            }
        }
        cout << "Image " << m << " was handled" << "\n";
    }
}

void Filter::OpenFunction(const DICOMImageCollection* collection, short kernelSize)
{
	ErosionFilter(collection, kernelSize);

	DilationFilter(collection, kernelSize);
}

void Filter::CloseFunction(const DICOMImageCollection* collection, short kernelSize)
{
	DilationFilter(collection, kernelSize);

	ErosionFilter(collection, kernelSize);
}

void Filter::OpenCloseFunction(const DICOMImageCollection* collection, short kernelSize)
{
	CloseFunction(collection, kernelSize);

	OpenFunction(collection, kernelSize);
}

void Filter::CloseOpenFunction(const DICOMImageCollection* collection, short kernelSize)
{
	OpenFunction(collection, kernelSize);

	CloseFunction(collection, kernelSize);
}

Array2D_Allocator<Float32> Filter::getGaussianKernel(short size, float sigma)
{
	//Create an array for storage convolution matrix
    Array2D_Allocator<Float32> arr(size, size);

	float x = 0, y = 0, sum = 0;
	float sigma_sq = sigma * sigma;

	//Filling coordinate values and getting element values of convolution matrix
    Float32** gauss_kernel = arr.Get();
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			x = -size / static_cast<float>(2 + i);
			y = -size / static_cast<float>(2 + j);
			gauss_kernel[i][j] = (float)((1.0 / 2.0*M_PI*sigma_sq)*exp(-(x*x + y*y) / 2.0*sigma_sq));
			sum += gauss_kernel[i][j];
		}
	}
	//Normalizing convolution matrix
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			gauss_kernel[i][j] /= sum;
		}
	}
	return arr;
}

Array3D_Allocator<Sint16> Filter::makeExtendedImages(const DICOMImageCollection* collection, short kernelSize)
{
    const DICOMImage* images = collection->GetImages();
    const Uint32 count = collection->GetCount();
	const Uint16 rows = images[0].GetRows();
	const Uint16 columns = images[0].GetColumns();

	short new_image_columns = columns + 2 * (kernelSize / 2);
	short new_image_rows = rows + 2 * (kernelSize / 2);

	//Make windows extended
    Array3D_Allocator<Sint16> arr(count, new_image_rows, new_image_columns);
    Sint16*** extended_images = arr.Get();

	for (int k = 0; k < count; ++k)
	{
		short** data = images[k].GetData();

		//Fill left lower square
		for (short i = (rows - kernelSize / 2); i < rows; ++i)
		{
			for (short j = 0; j < kernelSize / 2; ++j)
			{
				extended_images[k][i + 2 * (kernelSize / 2)][j] = data[i][j];
			}
		}

		//Fill right lower square
		for (short i = (rows - kernelSize / 2); i < rows; ++i)
		{
			for (short j = (columns - kernelSize / 2); j < columns; ++j)
			{
				extended_images[k][i + 2 * (kernelSize / 2)][j + 2 * (kernelSize / 2)] = data[i][j];
			}
		}

		//Fill field from left lower to right lower squares
		for (short i = (rows - kernelSize / 2); i < rows; ++i)
		{
			for (short j = 0; j < columns; ++j)
			{
				extended_images[k][i + 2 * (kernelSize / 2)][j + kernelSize / 2] = data[i][j];
			}
		}

		//Fill field from right upper to right lower squares  ��������� ������� �� ������� �������� �� ������� ������� ��������
		for (short i = 0; i < rows; ++i)
		{
			for (short j = (columns - kernelSize / 2); j < columns; ++j)
			{
				extended_images[k][i + kernelSize / 2][j + 2 * (kernelSize / 2)] = data[i][j];
			}
		}

		//Fill left upper square
		for (short i = 0; i < kernelSize / 2; ++i)
		{
			for (short j = 0; j < kernelSize / 2; ++j)
			{
				extended_images[k][i][j] = data[i][j];
			}
		}

		//Fill right upper square
		for (short i = 0; i < kernelSize / 2; ++i)
		{
			for (short j = (columns - kernelSize / 2); j < columns; ++j)
			{
				extended_images[k][i][j + 2 * (kernelSize / 2)] = data[i][j];
			}
		}

		//Fill field from left upper to right upper squares
		for (short i = 0; i < kernelSize / 2; ++i)
		{
			for (short j = 0; j < columns; ++j)
			{
				extended_images[k][i][j + kernelSize / 2] = data[i][j];
			}
		}

		//Fill field from left upper to left lower squares 
		for (short i = 0; i < rows; ++i)
		{
			for (short j = 0; j < kernelSize / 2; ++j)
			{
				extended_images[k][i + kernelSize / 2][j] = data[i][j];
			}
		}

		//Fill center
		for (short i = 0; i < rows; ++i)
		{
			for (short j = 0; j < columns; ++j)
			{
				extended_images[k][i + kernelSize / 2][j + kernelSize / 2] = data[i][j];
			}
		}
	}
	return arr;
}

void MedBox::Imaging::Filter::freeExtendedImages(short*** extended_images, short kernel_size, short rows, short count)
{
    Uint16 new_image_rows = rows + 2 * (kernel_size / 2);
    for (int k = 0; k < count; ++k)
    {
        for (int j = 0; j < new_image_rows; ++j)
            delete[] extended_images[k][j];

        delete[] extended_images[k];
    }
    delete[] extended_images;
}

bool Filter::kernelSizeIsValid(short kernel_size)
{
	//Check odd and non-negative
	if (kernel_size % 2 == 0 || kernel_size < 0)
		return false;

	return true;
}
