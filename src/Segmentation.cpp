#include "Segmentation.h"
#include "SegmentationType.h"
#include "DICOMImage.h"

using MedBox::Imaging::Segmentation;
using MedBox::Imaging::DICOMImage;

const void Segmentation::ThresholdSegmentation(DICOMImageCollection* collection, short t)
{
    collection->SegmentationMark = SEGMENTATION_TYPE::THRESHOLD;
    const DICOMImage* images = collection->GetImages();
    const Uint32 count = collection->GetCount();
    const Uint16 rows = images[0].GetRows();
    const Uint16 columns = images[0].GetColumns();

    for (Uint32 k = 0; k < count; ++k)
    {
        for (Uint16 j = 0; j < rows; ++j)
        {
            for (Uint16 i = 0; i < columns; ++i)
            {
                if (images[k].GetData()[j][i] > t)
                    images[k].GetData()[j][i] = 1;

                else
                    images[k].GetData()[j][i] = 0;
            }
        }
    }
}

const void Segmentation::SplitMergeSegmentation(DICOMImageCollection * collection)
{

}

const void Segmentation::WatershedSegmentation(DICOMImageCollection* collection)
{

}

const void Segmentation::RegionGrowingSegmentation(DICOMImageCollection* collection)
{

}