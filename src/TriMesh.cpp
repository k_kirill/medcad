#include "main.h"

#include <boost\algorithm\string.hpp>

#include "TriMesh.h"
#include "Exporter.h"

namespace M = MedBox::Modeling;

M::TriMesh::TriMesh() { }

M::TriMesh::TriMesh(const list<Tri> &triangles)
{
	vector<Tri> t{ std::make_move_iterator(std::begin(triangles)),
				   std::make_move_iterator(std::end(triangles)) };
	_triangles = t;

	CalculateQuality();
	Update();
}

M::TriMesh::~TriMesh() 
{
    _triangles.clear();
    Clear();
}

void M::TriMesh::Update()
{
	Clear();

	set_base_structures();

    set_edges_and_triangle_to_edges_indices();
    set_edge_to_triangle_indices();

	set_triangle_to_triangle_indices();
}

void M::TriMesh::set_base_structures()
{
	set<Vert> vertex_set;
	unsigned int tri_index = 0;

	//Detect unique _vertices and vertex_to_triangle_indices collections
	for (auto&& triangle = _triangles.begin(); triangle != _triangles.end(); ++triangle, ++tri_index)
	{
		for (size_t i = 0; i < 3; ++i)
		{
			set<Vert>::const_iterator it = vertex_set.find(triangle->v[i]);

			if (vertex_set.end() == it)
			{
				triangle->v[i].index = (int)_vertices.size();
				vertex_set.insert(triangle->v[i]);
				_vertices.push_back(triangle->v[i]);

				vector<Uint32> tri_indices;
				tri_indices.push_back(tri_index);
				_vertex_to_triangle_indices.push_back(tri_indices);
			}

			else
			{
				triangle->v[i].index = it->index;

				_vertex_to_triangle_indices[it->index].push_back(tri_index);
			}
		}
	}
	vertex_set.clear();
	//End detecting _vertices and vertex_to_triangle_indices

	//Detect vertex_to_vertex_indices collection
	_vertex_to_vertex_indices.resize(_vertices.size());

	for (size_t i = 0; i < _vertex_to_triangle_indices.size(); ++i)
	{
		set<unsigned int> vertex_to_vertex_indices_set;

		for (size_t j = 0; j < _vertex_to_triangle_indices[i].size(); ++j)
		{
			tri_index = _vertex_to_triangle_indices[i][j];

			for (size_t k = 0; k < 3; ++k)
			{
				if (i != _triangles[tri_index].v[k].index)
				{
					vertex_to_vertex_indices_set.insert(_triangles[tri_index].v[k].index);
				}
			}
		}

		for (auto iter = vertex_to_vertex_indices_set.begin(); iter != vertex_to_vertex_indices_set.end(); ++iter)
			_vertex_to_vertex_indices[i].push_back(*iter);
	}
	//End detecting vertex_to_vertex_indices
}

void M::TriMesh::set_edges_and_triangle_to_edges_indices()
{
	//Detect unique edges and triangle_to_edge_indices collections
	set<Edg> edge_set;
	for (const auto& triangle : _triangles)
	{
		Edg edges_3[3] = { Edg(triangle.v[0].index, triangle.v[1].index),
						   Edg(triangle.v[0].index, triangle.v[2].index),
						   Edg(triangle.v[1].index, triangle.v[2].index) };

		vector<Uint32> edge_indices;
		for (size_t j = 0; j < 3; ++j)
		{
			auto it = edge_set.find(edges_3[j]);

			if (it == edge_set.end())
			{
				edges_3[j].index = (int)_edges.size();
				edge_set.insert(edges_3[j]);
				_edges.push_back(edges_3[j]);

				edge_indices.push_back((Uint32)(_edges.size() - 1));
			}

			else
			{
				edge_indices.push_back(it->index);
			}
		}
		_triangle_to_edge_indices.push_back(edge_indices);
	}
	edge_set.clear();
	//End detecting unique _edges
}

void M::TriMesh::set_edge_to_triangle_indices()
{
	//Detect edge_to_triangle_indices collection
	for (unsigned int i = 0; i < _edges.size(); ++i)
	{
		unsigned int first = _edges[i].vertex_indices[0];
		unsigned int second = _edges[i].vertex_indices[1];

		vector<Uint32> tri_indices;

		for (size_t k = 0; k < _vertex_to_triangle_indices[first].size(); ++k)
		{
			for (size_t l = 0; l < _vertex_to_triangle_indices[second].size(); ++l)
			{
				unsigned int tri0_index = _vertex_to_triangle_indices[first][k];
				unsigned int tri1_index = _vertex_to_triangle_indices[second][l];

				if (tri0_index == tri1_index)
				{
					tri_indices.push_back(tri0_index);
					break;
				}
			}
		}
		_edge_to_triangle_indices.push_back(tri_indices);
	}
}

void M::TriMesh::set_triangle_to_triangle_indices()
{
	//Detect triangle_to_triangle_indices collection
	for (size_t i = 0; i < _triangles.size(); ++i)
	{
		vector<Uint32> edge_indices = _triangle_to_edge_indices[i];
		vector<Uint32> tri_indices;

		for (size_t j = 0; j < edge_indices.size(); ++j)
		{
			unsigned int edge_index = edge_indices[j];

			for (size_t k = 0; k < _edge_to_triangle_indices[edge_index].size(); ++k)
			{
				unsigned int tri_index = _edge_to_triangle_indices[edge_index][k];

				if (i != tri_index)
					tri_indices.push_back(tri_index);
			}
		}
		_triangle_to_triangle_indices.push_back(tri_indices);
	}
}

void M::TriMesh::GenerateTriangleNormals()
{
	for (Tri triangle : _triangles)
	{
		triangle.v[0] = _vertices[triangle.v[0].index];
		triangle.v[1] = _vertices[triangle.v[1].index];
		triangle.v[2] = _vertices[triangle.v[2].index];

		triangle.RecalculateTriangleNormal();
		triangle.normal.Normalize();
		triangle.RecalculateParameters();
	}
	CalculateQuality();
}

void M::TriMesh::CalculateQuality()
{
	Float64 sum = 0.0;
	for (const auto& triangle : _triangles)
		sum += triangle.quality;

	_quality = sum / static_cast<Float64>(_triangles.size());
}

const vector <Tri>& M::TriMesh::GetTriangleList() const
{
	return _triangles;
}

const vector<Vert>& M::TriMesh::GetVertices() const
{
	return _vertices;
}

const vector<Edg>& M::TriMesh::GetEdges() const
{
	return _edges;
}

const vector<vector<Uint32>>& M::TriMesh::GetVertexListToTriangleIndices() const
{
	return _vertex_to_triangle_indices;
}

const vector<vector<Uint32>>& M::TriMesh::GetVertexListToVertexIndices() const
{
	return _vertex_to_vertex_indices;
}

const vector<vector<Uint32>>& M::TriMesh::GetEdgeListToTriangleIndices() const
{
	return _edge_to_triangle_indices;
}

const vector<vector<Uint32>>& M::TriMesh::GetTriangleListToEdgeIndices() const
{
	return _triangle_to_edge_indices;
}

const vector<vector<Uint32>>& MedBox::Modeling::TriMesh::GetTriangleListToTriangleIndices() const
{
	return _triangle_to_triangle_indices;
}

Float64 M::TriMesh::GetMeshQuality() const
{
	return _quality;
}

void M::TriMesh::Clear()
{
	_vertices.clear();
	_vertex_to_triangle_indices.clear();
	_vertex_to_vertex_indices.clear();
	_edges.clear();
	_edge_to_triangle_indices.clear();
	_triangle_to_edge_indices.clear();
	_triangle_to_triangle_indices.clear();
}

OBJECT_TYPE M::TriMesh::GetType() const
{
	return _type;
}

bool MedBox::Modeling::TriMesh::VertexIsExist(const Vert & v) const
{
    for (const Vert& vertex : _vertices)
        if (vertex == v)
            return true;

    return false;
}

bool MedBox::Modeling::TriMesh::TriangleIsExist(const Tri & t) const
{
    for (const Tri& triangle : _triangles)
        if (t == triangle)
            return true;

    return false;
}

Uint32 MedBox::Modeling::TriMesh::GetFaceCount() const
{
	return static_cast<Uint32>(_triangles.size());
}

Uint32 MedBox::Modeling::TriMesh::GetVertexCount() const
{
	return static_cast<Uint32>(_vertices.size());
}

Uint32 MedBox::Modeling::TriMesh::GetEdgeCount() const
{
	return static_cast<Uint32>(_edges.size());
}