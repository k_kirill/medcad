#include "Builder.h"

using namespace MedBox::Modeling;

Builder::Builder(DICOMImageCollection* collection, bool standartMC, const short size, const short iso_surface)
{
    _collection = collection;
    _segm_type = collection->SegmentationMark;
    _standartMC = standartMC;
    _iso_surface = iso_surface;
    _cell_size = size;
}

void Builder::getVertices(short edge, short *arr) {
	switch (edge) {
	case 0:
		arr[0] = 0;
		arr[1] = 1;
		break;
	case 1:
		arr[0] = 1;
		arr[1] = 2;
		break;
	case 2:
		arr[0] = 2;
		arr[1] = 3;
		break;
	case 3:
		arr[0] = 3;
		arr[1] = 0;
		break;
	case 4:
		arr[0] = 4;
		arr[1] = 5;
		break;
	case 5:
		arr[0] = 5;
		arr[1] = 6;
		break;
	case 6:
		arr[0] = 6;
		arr[1] = 7;
		break;
	case 7:
		arr[0] = 7;
		arr[1] = 4;
		break;
	case 8:
		arr[0] = 0;
		arr[1] = 4;
		break;
	case 9:
		arr[0] = 1;
		arr[1] = 5;
		break;
	case 10:
		arr[0] = 2;
		arr[1] = 6;
		break;
	case 11:
		arr[0] = 3;
		arr[1] = 7;
		break;
	}
}

bool Builder::Build(Uint16 i, Uint16 j, Uint32 k, const Uint32 offset)
{
    const DICOMImage& current = _collection->Image(k);
    const DICOMImage& next = _collection->Image(k + 1);

    Float64 dx = current.GetX_PixSP();
    Float64 dy = current.GetY_PixSP();
    Float64 dz = next.GetSliceLoc() - current.GetSliceLoc();

    _cell.vertex[0] = Vert(               i * dx,                j * dy, (k + offset) * dz);
    _cell.vertex[1] = Vert((i + _cell_size) * dx,                j * dy, (k + offset) * dz);
    _cell.vertex[2] = Vert((i + _cell_size) * dx,                j * dy, (k + 1 + offset) * dz);
    _cell.vertex[3] = Vert(               i * dx,                j * dy, (k + 1 + offset) * dz);
    _cell.vertex[4] = Vert(               i * dx, (j + _cell_size) * dy, (k + offset) * dz);
    _cell.vertex[5] = Vert((i + _cell_size) * dx, (j + _cell_size) * dy, (k + offset) * dz);
    _cell.vertex[6] = Vert((i + _cell_size) * dx, (j + _cell_size) * dy, (k + 1 + offset) * dz);
    _cell.vertex[7] = Vert(               i * dx, (j + _cell_size) * dy, (k + 1 + offset) * dz);

	short count = 0;
	if (_segm_type != SEGMENTATION_TYPE::THRESHOLD)
	{
		if (_standartMC)
		{
			_cell.value[0] = current.GetData()[j][i];
			_cell.value[1] = current.GetData()[j][i + _cell_size];
			_cell.value[2] =    next.GetData()[j][i + _cell_size];
			_cell.value[3] =    next.GetData()[j][i];
			_cell.value[4] = current.GetData()[j + _cell_size][i];
			_cell.value[5] = current.GetData()[j + _cell_size][i + _cell_size];
			_cell.value[6] =    next.GetData()[j + _cell_size][i + _cell_size];
			_cell.value[7] =    next.GetData()[j + _cell_size][i];

			for (size_t i = 0; i < 8; ++i)
			{
				_cell.nodeParity[i] = _cell.value[i] > _iso_surface;

				if (_cell.nodeParity[i])
					count++;
			}
		}

		else
		{
			_cell.value[0] = current.GetData()[j][i]                           - _iso_surface;
			_cell.value[1] = current.GetData()[j][i + _cell_size]              - _iso_surface;
			_cell.value[2] =    next.GetData()[j][i + _cell_size]              - _iso_surface;
			_cell.value[3] =    next.GetData()[j][i]                           - _iso_surface;
			_cell.value[4] = current.GetData()[j + _cell_size][i]              - _iso_surface;
			_cell.value[5] = current.GetData()[j + _cell_size][i + _cell_size] - _iso_surface;
			_cell.value[6] =    next.GetData()[j + _cell_size][i + _cell_size] - _iso_surface;
			_cell.value[7] =    next.GetData()[j + _cell_size][i]              - _iso_surface;

			for (size_t i = 0; i < 8; ++i)
			{
				_cell.nodeParity[i] = _cell.value[i] > 0;

				if (_cell.nodeParity[i])
					count++;
			}
		}
	}

	else
	{
		_cell.value[0] = current.GetData()[j][i];
		_cell.value[1] = current.GetData()[j][i + _cell_size];
		_cell.value[2] =    next.GetData()[j][i + _cell_size];
		_cell.value[3] =    next.GetData()[j][i];
		_cell.value[4] = current.GetData()[j + _cell_size][i];
		_cell.value[5] = current.GetData()[j + _cell_size][i + _cell_size];
		_cell.value[6] =    next.GetData()[j + _cell_size][i + _cell_size];
		_cell.value[7] =    next.GetData()[j + _cell_size][i];

		for (size_t i = 0; i < 8; ++i)
		{
			_cell.nodeParity[i] = _cell.value[i] > 0;

			if (_cell.nodeParity[i])
				count++;
		}
	}

	if (count > 0 && count < 8)
		return true;

	else
		return false;
}

Vert Builder::getIntersection(short edge)
{
	if (edge == 12)
		return _cell.additional_vertex;

	short vertices[2];
	getVertices(edge, vertices);

	Vert v1 = _cell.vertex[vertices[0]];
	Vert v2 = _cell.vertex[vertices[1]];

	short value1 = _cell.value[vertices[0]];
	short value2 = _cell.value[vertices[1]];

	Vert v;
	float scale;

	switch (_segm_type)
	{
    case SEGMENTATION_TYPE::NONE:
	{
		if (_standartMC)
		{
			if (abs(_iso_surface - value1) == 0)
				return v1;
			if (abs(_iso_surface - value2) == 0)
				return v2;

			scale = (_iso_surface - value1) / static_cast<float>(value2 - value1);
		}

		else
		{
			if (abs(value1) == 0)
				return v1;
			if (abs(value2) == 0)
				return v2;

			scale = -value1 / static_cast<float>(value2 - value1);
		}

		if (abs(value1 - value2) == 0)
			return v1;
		break;
	}

    case SEGMENTATION_TYPE::THRESHOLD:
	{
		scale = 0.5;
		break;
	}

	default:
		break;
	}

	v = v1 + (v2 - v1) * scale;

	return v;
}

short Builder::getNodeCaseNumber()
{
	short powerOf2 = 1;
	short caseID = 0;
	for (size_t i = 0; i < 8; i++)
	{
		if (_cell.nodeParity[i])
			caseID += powerOf2;

		powerOf2 *= 2;
	}
	return caseID;
}

void Builder::addTriangles(list<Tri> &triangles, const __int8 edges[], short triangles_count)
{
	short index = 0;
	for (size_t i = 0; i < triangles_count; ++i)
	{
		Vert v[3];

		v[0] = getIntersection(edges[index]);
		v[1] = getIntersection(edges[index + 1]);
		v[2] = getIntersection(edges[index + 2]);

		Tri triangle(v);
		triangle.normal.Normalize();
		index += 3;

		if (triangle.v[0] == triangle.v[1] ||
			triangle.v[0] == triangle.v[2] ||
			triangle.v[1] == triangle.v[2])
			continue;

		triangles.push_back(triangle);
	}
}

list<Tri> Builder::getTriangles()
{
	list<Tri> triangles;
	short index = 0;
	short nodeCase = getNodeCaseNumber();

	if (_standartMC)
	{
		while (index < 16 && classicCases[nodeCase][index] != -1)
		{
			Vert v[3];

			v[0] = getIntersection(classicCases[nodeCase][index]);
			v[1] = getIntersection(classicCases[nodeCase][index + 1]);
			v[2] = getIntersection(classicCases[nodeCase][index + 2]);

			Tri triangle(v);
			triangle.normal.Normalize();
			index += 3;

			if (triangle.v[0] == triangle.v[1] ||
				triangle.v[0] == triangle.v[2] ||
				triangle.v[1] == triangle.v[2])
				continue;

			triangles.push_back(triangle);
		}
		return triangles;
	}

	//MC33 begining
	_case = cases[nodeCase][0];
	_config = cases[nodeCase][1];
	_subconfig = 0;

	switch (_case)
	{
	case  0:
		break;

	case  1:
		addTriangles(triangles, tiling1[_config], 1);
		break;

	case  2:
		addTriangles(triangles, tiling2[_config], 2);
		break;

	case  3:
		if (testFace(test3[_config]))
			addTriangles(triangles, tiling3_2[_config], 4); // 3.2
		else
			addTriangles(triangles, tiling3_1[_config], 2); // 3.1
		break;

	case  4:
		if (testInterior(test4[_config]))
			addTriangles(triangles, tiling4_1[_config], 2); // 4.1.1
		else
			addTriangles(triangles, tiling4_2[_config], 6); // 4.1.2
		break;

	case  5:
		addTriangles(triangles, tiling5[_config], 3);
		break;

	case  6:
		if (testFace(test6[_config][0]))
			addTriangles(triangles, tiling6_2[_config], 5); // 6.2
		else
		{
			if (testInterior(test6[_config][1]))
				addTriangles(triangles, tiling6_1_1[_config], 3); // 6.1.1
			else
			{
				addAdditionalVertex();
				addTriangles(triangles, tiling6_1_2[_config], 9); // 6.1.2
			}
		}
		break;

	case  7:
		if (testFace(test7[_config][0])) _subconfig += 1;
		if (testFace(test7[_config][1])) _subconfig += 2;
		if (testFace(test7[_config][2])) _subconfig += 4;
		switch (_subconfig)
		{
		case 0:
			addTriangles(triangles, tiling7_1[_config], 3); break;
		case 1:
			addTriangles(triangles, tiling7_2[_config][0], 5); break;
		case 2:
			addTriangles(triangles, tiling7_2[_config][1], 5); break;
		case 3:
			addAdditionalVertex();
			addTriangles(triangles, tiling7_3[_config][0], 9); break;
		case 4:
			addTriangles(triangles, tiling7_2[_config][2], 5); break;
		case 5:
			addAdditionalVertex();
			addTriangles(triangles, tiling7_3[_config][1], 9); break;
		case 6:
			addAdditionalVertex();
			addTriangles(triangles, tiling7_3[_config][2], 9); break;
		case 7:
			if (testInterior(test7[_config][3]))
				addTriangles(triangles, tiling7_4_2[_config], 9);
			else
				addTriangles(triangles, tiling7_4_1[_config], 5);
			break;
		};
		break;

	case  8:
		addTriangles(triangles, tiling8[_config], 2);
		break;

	case  9:
		addTriangles(triangles, tiling9[_config], 4);
		break;

	case 10:
		if (testFace(test10[_config][0]))
		{
			if (testFace(test10[_config][1]))
				addTriangles(triangles, tiling10_1_1_[_config], 4); // 10.1.1
			else
			{
				addAdditionalVertex();
				addTriangles(triangles, tiling10_2[_config], 8); // 10.2
			}
		}
		else
		{
			if (testFace(test10[_config][1]))
			{
				addAdditionalVertex();
				addTriangles(triangles, tiling10_2_[_config], 8); // 10.2
			}
			else
			{
				if (testInterior(test10[_config][2]))
					addTriangles(triangles, tiling10_1_1[_config], 4); // 10.1.1
				else
					addTriangles(triangles, tiling10_1_2[_config], 8); // 10.1.2
			}
		}
		break;

	case 11:
		addTriangles(triangles, tiling11[_config], 4);
		break;

	case 12:
		if (testFace(test12[_config][0]))
		{
			if (testFace(test12[_config][1]))
				addTriangles(triangles, tiling12_1_1_[_config], 4); // 12.1.1
			else
			{
				addAdditionalVertex();
				addTriangles(triangles, tiling12_2[_config], 8); // 12.2
			}
		}
		else
		{
			if (testFace(test12[_config][1]))
			{
				addAdditionalVertex();
				addTriangles(triangles, tiling12_2_[_config], 8); // 12.2
			}
			else
			{
				if (testInterior(test12[_config][2]))
					addTriangles(triangles, tiling12_1_1[_config], 4); // 12.1.1
				else
					addTriangles(triangles, tiling12_1_2[_config], 8); // 12.1.2
			}
		}
		break;

	case 13:
		if (testFace(test13[_config][0])) _subconfig += 1;
		if (testFace(test13[_config][1])) _subconfig += 2;
		if (testFace(test13[_config][2])) _subconfig += 4;
		if (testFace(test13[_config][3])) _subconfig += 8;
		if (testFace(test13[_config][4])) _subconfig += 16;
		if (testFace(test13[_config][5])) _subconfig += 32;
		switch (subconfig13[_subconfig])
		{
		case 0:/* 13.1 */
			addTriangles(triangles, tiling13_1[_config], 4); break;

		case 1:/* 13.2 */
			addTriangles(triangles, tiling13_2[_config][0], 6); break;
		case 2:/* 13.2 */
			addTriangles(triangles, tiling13_2[_config][1], 6); break;
		case 3:/* 13.2 */
			addTriangles(triangles, tiling13_2[_config][2], 6); break;
		case 4:/* 13.2 */
			addTriangles(triangles, tiling13_2[_config][3], 6); break;
		case 5:/* 13.2 */
			addTriangles(triangles, tiling13_2[_config][4], 6); break;
		case 6:/* 13.2 */
			addTriangles(triangles, tiling13_2[_config][5], 6); break;

		case 7:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][0], 10); break;
		case 8:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][1], 10); break;
		case 9:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][2], 10); break;
		case 10:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][3], 10); break;
		case 11:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][4], 10); break;
		case 12:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][5], 10); break;
		case 13:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][6], 10); break;
		case 14:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][7], 10); break;
		case 15:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][8], 10); break;
		case 16:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][9], 10); break;
		case 17:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][10], 10); break;
		case 18:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3[_config][11], 10); break;

		case 19:/* 13.4 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_4[_config][0], 12); break;
		case 20:/* 13.4 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_4[_config][1], 12); break;
		case 21:/* 13.4 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_4[_config][2], 12); break;
		case 22:/* 13.4 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_4[_config][3], 12); break;

		case 23:/* 13.5 */
			_subconfig = 0;
			if (testInterior(test13[_config][6]))
				addTriangles(triangles, tiling13_5_1[_config][0], 6);
			else
				addTriangles(triangles, tiling13_5_2[_config][0], 10);
			break;
		case 24:/* 13.5 */
			_subconfig = 1;
			if (testInterior(test13[_config][6]))
				addTriangles(triangles, tiling13_5_1[_config][1], 6);
			else
				addTriangles(triangles, tiling13_5_2[_config][1], 10);
			break;
		case 25:/* 13.5 */
			_subconfig = 2;
			if (testInterior(test13[_config][6]))
				addTriangles(triangles, tiling13_5_1[_config][2], 6);
			else
				addTriangles(triangles, tiling13_5_2[_config][2], 10);
			break;
		case 26:/* 13.5 */
			_subconfig = 3;
			if (testInterior(test13[_config][6]))
				addTriangles(triangles, tiling13_5_1[_config][3], 6);
			else
				addTriangles(triangles, tiling13_5_2[_config][3], 10);
			break;

		case 27:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][0], 10); break;
		case 28:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][1], 10); break;
		case 29:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][2], 10); break;
		case 30:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][3], 10); break;
		case 31:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][4], 10); break;
		case 32:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][5], 10); break;
		case 33:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][6], 10); break;
		case 34:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][7], 10); break;
		case 35:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][8], 10); break;
		case 36:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][9], 10); break;
		case 37:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][10], 10); break;
		case 38:/* 13.3 */
			addAdditionalVertex();
			addTriangles(triangles, tiling13_3_[_config][11], 10); break;

		case 39:/* 13.2 */
			addTriangles(triangles, tiling13_2_[_config][0], 6); break;
		case 40:/* 13.2 */
			addTriangles(triangles, tiling13_2_[_config][1], 6); break;
		case 41:/* 13.2 */
			addTriangles(triangles, tiling13_2_[_config][2], 6); break;
		case 42:/* 13.2 */
			addTriangles(triangles, tiling13_2_[_config][3], 6); break;
		case 43:/* 13.2 */
			addTriangles(triangles, tiling13_2_[_config][4], 6); break;
		case 44:/* 13.2 */
			addTriangles(triangles, tiling13_2_[_config][5], 6); break;

		case 45:/* 13.1 */
			addTriangles(triangles, tiling13_1_[_config], 4); break;

		default:
			std::cout << "Marching _cell.values: Impossible case 13?\n";
		}
		break;

	case 14:
		addTriangles(triangles, tiling14[_config], 4);
		break;
	};

	return triangles;
}

bool Builder::testFace(__int8 face)
{
	short A, B, C, D;

	switch (face) {
	case -1:
	case 1:
		A = _cell.value[0];
		B = _cell.value[4];
		C = _cell.value[5];
		D = _cell.value[1];
		break;
	case -2:
	case 2:
		A = _cell.value[1];
		B = _cell.value[5];
		C = _cell.value[6];
		D = _cell.value[2];
		break;
	case -3:
	case 3:
		A = _cell.value[2];
		B = _cell.value[6];
		C = _cell.value[7];
		D = _cell.value[3];
		break;
	case -4:
	case 4:
		A = _cell.value[3];
		B = _cell.value[7];
		C = _cell.value[4];
		D = _cell.value[0];
		break;
	case -5:
	case 5:
		A = _cell.value[0];
		B = _cell.value[3];
		C = _cell.value[2];
		D = _cell.value[1];
		break;
	case -6:
	case 6:
		A = _cell.value[4];
		B = _cell.value[7];
		C = _cell.value[6];
		D = _cell.value[5];
		break;

	default:
		cout << "Invalid face code " << face << "\n";
		A = B = C = D = 0;
		break;
	};

	if (fabs(A * C - B * D) < 0.1)
		return face >= 0;

	return face * A * (A * C - B * D) >= 0; // face and A invert signs
}

bool Builder::testInterior(__int8 s)
{
	short a, b;
	float t;
	float At = 0, Bt = 0, Ct = 0, Dt = 0;
	char  test = 0;
	char  edge = -1; // reference edge of the triangulation

	switch (_case)
	{
	case  4:
	case 10:
		a = (_cell.value[4] - _cell.value[0]) * (_cell.value[6] - _cell.value[2]) - 
            (_cell.value[7] - _cell.value[3]) * (_cell.value[5] - _cell.value[1]);

		b = _cell.value[2] * (_cell.value[4] - _cell.value[0]) +
            _cell.value[0] * (_cell.value[6] - _cell.value[2]) -
            _cell.value[1] * (_cell.value[7] - _cell.value[3]) - 
            _cell.value[3] * (_cell.value[5] - _cell.value[1]);

		t = -b / static_cast<float>(2 * a);
		if (t<0 || t>1) return s>0;

		At = _cell.value[0] + (_cell.value[4] - _cell.value[0]) * t;
		Bt = _cell.value[3] + (_cell.value[7] - _cell.value[3]) * t;
		Ct = _cell.value[2] + (_cell.value[6] - _cell.value[2]) * t;
		Dt = _cell.value[1] + (_cell.value[5] - _cell.value[1]) * t;
		break;

	case  6:
	case  7:
	case 12:
	case 13:
		switch (_case)
		{
		case  6: edge = test6[_config][2]; break;
		case  7: edge = test7[_config][4]; break;
		case 12: edge = test12[_config][3]; break;
		case 13: edge = tiling13_5_1[_config][_subconfig][0]; break;
		}
		switch (edge)
		{
		case  0:
			t = _cell.value[0] / static_cast<float>(_cell.value[0] - _cell.value[1]);
			At = 0;
			Bt = _cell.value[3] + (_cell.value[2] - _cell.value[3]) * t;
			Ct = _cell.value[7] + (_cell.value[6] - _cell.value[7]) * t;
			Dt = _cell.value[4] + (_cell.value[5] - _cell.value[4]) * t;
			break;
		case  1:
			t = _cell.value[1] / static_cast<float>(_cell.value[1] - _cell.value[2]);
			At = 0;
			Bt = _cell.value[0] + (_cell.value[3] - _cell.value[0]) * t;
			Ct = _cell.value[4] + (_cell.value[7] - _cell.value[4]) * t;
			Dt = _cell.value[5] + (_cell.value[6] - _cell.value[5]) * t;
			break;
		case  2:
			t = _cell.value[2] / static_cast<float>(_cell.value[2] - _cell.value[3]);
			At = 0;
			Bt = _cell.value[1] + (_cell.value[0] - _cell.value[1]) * t;
			Ct = _cell.value[5] + (_cell.value[4] - _cell.value[5]) * t;
			Dt = _cell.value[6] + (_cell.value[7] - _cell.value[6]) * t;
			break;
		case  3:
			t = _cell.value[3] / static_cast<float>(_cell.value[3] - _cell.value[0]);
			At = 0;
			Bt = _cell.value[2] + (_cell.value[1] - _cell.value[2]) * t;
			Ct = _cell.value[6] + (_cell.value[5] - _cell.value[6]) * t;
			Dt = _cell.value[7] + (_cell.value[4] - _cell.value[7]) * t;
			break;
		case  4:
			t = _cell.value[4] / static_cast<float>(_cell.value[4] - _cell.value[5]);
			At = 0;
			Bt = _cell.value[7] + (_cell.value[6] - _cell.value[7]) * t;
			Ct = _cell.value[3] + (_cell.value[2] - _cell.value[3]) * t;
			Dt = _cell.value[0] + (_cell.value[1] - _cell.value[0]) * t;
			break;
		case  5:
			t = _cell.value[5] / static_cast<float>(_cell.value[5] - _cell.value[6]);
			At = 0;
			Bt = _cell.value[4] + (_cell.value[7] - _cell.value[4]) * t;
			Ct = _cell.value[0] + (_cell.value[3] - _cell.value[0]) * t;
			Dt = _cell.value[1] + (_cell.value[2] - _cell.value[1]) * t;
			break;
		case  6:
			t = _cell.value[6] / static_cast<float>(_cell.value[6] - _cell.value[7]);
			At = 0;
			Bt = _cell.value[5] + (_cell.value[4] - _cell.value[5]) * t;
			Ct = _cell.value[1] + (_cell.value[0] - _cell.value[1]) * t;
			Dt = _cell.value[2] + (_cell.value[3] - _cell.value[2]) * t;
			break;
		case  7:
			t = _cell.value[7] / static_cast<float>(_cell.value[7] - _cell.value[4]);
			At = 0;
			Bt = _cell.value[6] + (_cell.value[5] - _cell.value[6]) * t;
			Ct = _cell.value[2] + (_cell.value[1] - _cell.value[2]) * t;
			Dt = _cell.value[3] + (_cell.value[0] - _cell.value[3]) * t;
			break;
		case  8:
			t = _cell.value[0] / static_cast<float>(_cell.value[0] - _cell.value[4]);
			At = 0;
			Bt = _cell.value[3] + (_cell.value[7] - _cell.value[3]) * t;
			Ct = _cell.value[2] + (_cell.value[6] - _cell.value[2]) * t;
			Dt = _cell.value[1] + (_cell.value[5] - _cell.value[1]) * t;
			break;
		case  9:
			t = _cell.value[1] / static_cast<float>(_cell.value[1] - _cell.value[5]);
			At = 0;
			Bt = _cell.value[0] + (_cell.value[4] - _cell.value[0]) * t;
			Ct = _cell.value[3] + (_cell.value[7] - _cell.value[3]) * t;
			Dt = _cell.value[2] + (_cell.value[6] - _cell.value[2]) * t;
			break;
		case 10:
			t = _cell.value[2] / static_cast<float>(_cell.value[2] - _cell.value[6]);
			At = 0;
			Bt = _cell.value[1] + (_cell.value[5] - _cell.value[1]) * t;
			Ct = _cell.value[0] + (_cell.value[4] - _cell.value[0]) * t;
			Dt = _cell.value[3] + (_cell.value[7] - _cell.value[3]) * t;
			break;
		case 11:
			t = _cell.value[3] / static_cast<float>(_cell.value[3] - _cell.value[7]);
			At = 0;
			Bt = _cell.value[2] + (_cell.value[6] - _cell.value[2]) * t;
			Ct = _cell.value[1] + (_cell.value[5] - _cell.value[1]) * t;
			Dt = _cell.value[0] + (_cell.value[4] - _cell.value[0]) * t;
			break;
		default: std::cout << " Invalid edge " << edge << "\n";  break;
		}
		break;

	default: std::cout << " Invalid ambiguous case " << _case << "\n";  break;
	}

	if (At >= 0) test++;
	if (Bt >= 0) test += 2;
	if (Ct >= 0) test += 4;
	if (Dt >= 0) test += 8;
	switch (test)
	{
	case  0: return s>0;
	case  1: return s>0;
	case  2: return s>0;
	case  3: return s>0;
	case  4: return s>0;
	case  5: if (At * Ct - Bt * Dt < std::numeric_limits<float>::epsilon()) return s > 0; break;
	case  6: return s>0;
	case  7: return s<0;
	case  8: return s>0;
	case  9: return s>0;
	case 10: if (At * Ct - Bt * Dt >= std::numeric_limits<float>::epsilon()) return s > 0; break;
	case 11: return s<0;
	case 12: return s>0;
	case 13: return s<0;
	case 14: return s<0;
	case 15: return s<0;
	}

	return s < 0;
}