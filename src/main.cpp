#include "main.h"

#include "Filter.h"
#include "MeshGenerator.h"
#include "Exporter.h"
#include "Importer.h"
#include "TriMesh.h"
#include "TriMeshSimplification.h"
#include "TriMeshRepairing.h"
#include "TriMeshSmoothing.h"

using namespace MedBox;

using Imaging::DICOMImageCollection;
using Imaging::Filter;
using IO::Exporter;
using IO::Importer;
using Modeling::TriMeshSmoothing;
using Modeling::TriMeshSimplification;
using Modeling::TriMeshRepairing;
using Modeling::MeshGenerator;

int main(int argc, char* argv[])
{
    //Getting count file in current directory
    auto count = std::count_if(directory_iterator(argv[1]), directory_iterator(), static_cast<bool(*)(const path&)>(is_regular_file));
    std::cout << "Images was found: " << count << "\n";

    DICOMImageCollection* collection = new DICOMImageCollection((Uint32)count);
	Importer<DICOMImageCollection>::ImportDICOMImages(collection, argv);

    ///Put collection into multithread handling
    {
        ///Solo thread image hadling
        //Filter::GaussianFilter(collection, 5, 0.4f);

        ///Multi thread image hadling
        Uint16 coll_count = thread::hardware_concurrency();
        vector<DICOMImageCollection*> collections = DICOMImageCollection::Split(collection, coll_count);

        vector<boost::thread> workers;
        for (int i = 0; i < coll_count; ++i)
            workers.emplace_back(boost::bind<void>(&Filter::GaussianFilter, collections[i], 5, 0.4f));

        std::for_each(workers.begin(), workers.end(), [](boost::thread& worker) -> void
        {
            worker.join();
        });

        ///Solo thread model building
        //Modeling::TriMesh* mesh = MeshGenerator::BuildModel(collection, false, GridCell::Size::Four, 150);

        ///Multi thread model building
        Modeling::TriMesh* mesh = MeshGenerator::BuildModel(collections, false, GridCell::Four, 150);
        collection = DICOMImageCollection::Merge(collections);

        ///Exporting model
        Exporter<Modeling::TriMesh>::exportToPLY(mesh, "C:/Developer/Workspace/medcad/models/feet_multi.ply", false);
        cout << "Ok\n";

        delete mesh;
    }
    ///----------------------------------------------------------
    ///Test exporting to DICOM
    DICOMImage img = collection->GetImages()[0];
    Exporter<DICOMImage>::exportToDICOM(&img, "C:/Developer/Workspace/medcad/DICOM/test.dcm");
    ///------------------------------------------------

	//Sint16 iso_surface = 150;

	////Building model
	//shared_ptr <M::MarchingCube> marching_cube(new M::MarchingCube(collection, false, M::GridCell::Size::Four));
	//marching_cube->Start(iso_surface);
	//unique_ptr<M::TriMesh> mesh = marching_cube->GetMesh();

	//Exporter<M::TriMesh>::exportToPLY(mesh.get(), "D:/Study/Term_project/Models/Spine.ply", false);

	////Simplify mesh
	//shared_ptr<Decimator> decimator(new Decimator(mesh.get()));
	//decimator->SetReduceFraction(0.1f);
	//decimator->Start();

	//Exporter<M::TriMesh>::exportToPLY(mesh.get(), "D:/Study/Term_project/Models/Spine_simpl.ply", false);

	////Using Taubin smooth algorithm for model with Fujiwara operator
	//shared_ptr<Smoother> smoother(new Smoother(mesh.get()));
	//smoother->Start(0.55f, -0.6f, 15);

	////Writing smoothed mesh
	//Exporter<M::TriMesh>::exportToPLY(mesh.get(), "D:/Study/Term_project/Models/Spine_smooth.ply", false);

	//collection->~DICOMImageCollection();
	//marching_cube.reset();
	////service.reset();
	//mesh.reset();
	//decimator.reset();
	//smoother.reset();

    delete collection;
    std::this_thread::sleep_for(3s);
	cout << "Working have been finished!\n";
	getch();
	return 0;
}
