#pragma once

#ifndef MESHCONVERTER_H
#define MESHCONVERTER_H

#include "VCGTypes.h"
#include "TriMesh.h"

#include "main.h"

using namespace VCGTypes;

using Tri = MedBox::Modeling::Geometry::Triangle;
using Vert = MedBox::Modeling::Geometry::Vertex;

namespace MedBox
{
	namespace Modeling
	{
		template<class Obj>
		class MeshConverter
		{
		public:
			static bool ConvertToVCGTriMesh(Obj* mesh, VCGMesh& vcg_mesh)
			{
				vector<Tri> triangles = mesh->GetTriangleList();
				vector<Vert> vertices = mesh->GetVertices();

				if (triangles.size() != 0)
				{
					auto tcount = triangles.size();
					auto vcount = vertices.size();

					VCGMesh::VertexIterator vi = vcg::tri::Allocator<VCGMesh>::AddVertices(vcg_mesh, vcount);
					VCGMesh::FaceIterator fi = vcg::tri::Allocator<VCGMesh>::AddFaces(vcg_mesh, tcount);

					VCGMesh::VertexPointer ivp[4];

					//Create vertices
					for (Vert v : vertices)
					{
						vi->P() = VCGMesh::CoordType(static_cast<Float32>(v.x),
                                                     static_cast<Float32>(v.y), 
                                                     static_cast<Float32>(v.z));
                        ++vi;
					}

					//Create faces
					for (Tri t : triangles)
					{
						for (int i = 0; i < 3; ++i)
						{
							int ind = t.v[i].index;

							vi = vcg_mesh.vert.begin() + ind; ivp[i] = &*vi;
							fi->V(i) = ivp[i];
						}
						++fi;
					}
				}

				else
				{
					cout << "Error: model is empty!\n";
					return false;
				}
				return true;
			}

			static bool ConvertToTriMesh(const VCGMesh& vcg_mesh, Obj* mesh)
			{
				if (vcg_mesh.FN() == 0) { cout << "Error: vcg mesh is empty!\n"; return false; }

				vector<Tri> triangles = mesh->GetTriangleList();
				triangles.clear();

				for (VCGMesh::ConstFaceIterator fi = vcg_mesh.face.begin(); fi != vcg_mesh.face.end(); ++fi)
				{
					Vert v[3];
					for (int j = 0; j < 3; ++j)
					{
						VCGMesh::CoordType coord = fi->cP(j);

						v[j] = Vert(coord.X(), coord.Y(), coord.Z());
					}
					Tri triangle(v);
					triangle.normal.Normalize();

					triangles.push_back(triangle);
				}
				mesh->Update();
				return true;
			}
		};
	}
}

#endif // !MESHCONVERTER_H

