#pragma once

#ifndef TRIMESHSERVICE_H
#define TRIMESHSERVICE_H

#include "main.h"

#include "TriMesh.h"
#include "VCGTypes.h"

namespace M = MedBox::Modeling;

namespace MedBox
{
	namespace Modeling
	{
        class TriMeshRepairing
        {
        public:
            const static void RepairAll(M::TriMesh*);
            const static void RemoveTrianglesFromFans(M::TriMesh*);
            const static void RemoveSelfIntersection(M::TriMesh*);
            const static void RemoveSharedTrianglesByEdge(M::TriMesh*);

        private:
            const static bool NonManifoldEdgesIsExist(M::TriMesh*);
            const static bool NonManifoldVerticesIsExist(M::TriMesh*);
            const static bool SelfIntersectionIsExist(M::TriMesh*       , 
                                                      VCGTypes::VCGMesh&,
                                                      vector<VCGTypes::VCGFace*>&);
        };
	}
}

#endif // !TRIMESHSERVICE_H

