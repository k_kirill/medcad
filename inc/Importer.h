#pragma once

#ifndef IMPORTER_H
#define IMPORTER_H

#include "main.h"

#include "TriMesh.h"
#include "VCGTypes.h"
#include "Format.h"
#include "MeshConverter.h"
#include "DICOMImageCollection.h"

//Boost lib
#include <boost\filesystem.hpp>
#include <boost\lambda\bind.hpp>

//VCG lib
#include <wrap\io_trimesh\import_stl.h>
#include <wrap\io_trimesh\import_ply.h>
#include <wrap\io_trimesh\import_off.h>
#include <wrap\io_trimesh\import_obj.h>
#include <wrap/io_trimesh/io_mask.h>

using namespace std;
using namespace VCGTypes;
using namespace vcg::tri;

using namespace boost::filesystem;
using namespace boost::lambda;

using MedBox::Modeling::Geometry::Triangle;
using MedBox::Modeling::MeshConverter;
using MedBox::Imaging::DICOMImageCollection;
using MedBox::Imaging::DICOMImage;

namespace MedBox
{
	namespace IO
	{
		template<class Obj>
		class Importer
		{
		public:
			//Import DICOM slices
			static void ImportDICOMImages(Obj* collection, char* argv[])
			{
				for (recursive_directory_iterator it(argv[1]), end; it != end; it++)
				{
					const string file_name = it->path().string();
                    DICOMImage image(file_name.c_str());
                    collection->Add(image);
				}
			}

			//Import from STL
			static bool ImportFromSTL(Obj* obj, const char* path)
			{
				if (load(obj, path, FORMAT::STL)) return true;

				else { cout << "Error happened while loading file:" << path << "\n"; return false; }
			}

			//Import from PLY
			static bool ImportFromPLY(Obj* obj, const char* path)
			{
				if (load(obj, path, FORMAT::PLY)) return true;

				else { cout << "Error happened while loading file:" << path << "\n"; return false; }
			}

			//Import from OFF
			static bool ImportFromOFF(Obj* obj, const char* path)
			{
				if (load(obj, path, FORMAT::OFF)) return true;

				else { cout << "Error happened while loading file:" << path << "\n"; return false; }
			}

			//Import from OBJ
			static bool ImportFromOBJ(Obj* obj, const char* path)
			{
				if (load(obj, path, FORMAT::OBJ)) return true;

				else { cout << "Error happened while loading file:" << path << "\n"; return false; }
			}

		private:
			//Load function
			static bool load(Obj* obj, const char* path, FORMAT format)
			{
				int success = -1;
				VCGMesh mesh;

				switch (format)
				{
				case FORMAT::STL:
				{
					Sint32 mask = io::Mask::IOM_FACEINDEX |
								  io::Mask::IOM_VERTCOORD;
					success = io::ImporterSTL<VCGMesh>::Open(mesh, path, mask);
				} break;

				case FORMAT::PLY:
				{
					Sint32 mask = io::Mask::IOM_FACEINDEX |
							      io::Mask::IOM_VERTCOORD;
					success = io::ImporterPLY<VCGMesh>::Open(mesh, path, mask);
				} break;

				case FORMAT::OFF:
				{
					Sint32 mask = io::Mask::IOM_FACEINDEX |
							      io::Mask::IOM_VERTCOORD;
					success = io::ImporterOFF<VCGMesh>::Open(mesh, path, mask);
                } break;

				case FORMAT::OBJ:
				{
					Sint32 mask = io::Mask::IOM_NONE;
					success = io::ImporterOBJ<VCGMesh>::Open(mesh, path, mask);
                } break;

				default:
					return false;
				}

				if (success == io::ImporterSTL<VCGMesh>::E_NOERROR ||
					success == io::ImporterOFF<VCGMesh>::NoError ||
					success == io::ImporterOBJ<VCGMesh>::E_NOERROR ||
					success == ply::E_NOERROR)
				{
					//Convert here
					if (!MeshConverter<Obj>::ConvertToTriMesh(mesh, obj))
						return false;

					mesh.Clear();
					return true;
				}

				else 
                    return false;
			}
		};
	}
}

#endif // !IMPORTER_H

