#pragma once

#ifndef ARRAY_H
#define ARRAY_H

#include "IArrayAllocator.h"
#include "main.h"

namespace MedBox
{
    namespace Utils
    {
        template<class ArrayType>
        class Array1D_Allocator : public IArrayAllocator
        {
        public:
            Array1D_Allocator() { }
            Array1D_Allocator(Uint32 size) : _size(size)
            {
                free();
                allocate();
            }

            Array1D_Allocator(const Array1D_Allocator& arr)
            {
                _size = arr.Length();

                free();
                allocate();

                CopyData(arr.Get(), _size);
            }

            ~Array1D_Allocator()
            {
                free();
            }

            ArrayType* Get() const
            {
                return _array;
            }

            Uint32 Length() const
            {
                return _size;
            }

            void CopyData(ArrayType* arr, Uint32 size)
            {
                if (_size == size && size != 0)
                {
                    for (Uint32 i = 0; i < _size; ++i)
                        _array[i] = arr[i];
                }
            }

            inline Array1D_Allocator<ArrayType>& Array1D_Allocator::operator=(const Array1D_Allocator<ArrayType> &arr)
            {
                _size = arr.Length();
                
                free();
                allocate();
                CopyData(arr.Get(), _size);

                return *this;
            }

        private:
            void allocate() override
            {
                if(_size != 0)
                    _array = new ArrayType[_size];
            }

            void free() override
            {
                if (_array != nullptr)
                {
                    delete[] _array;
                    _array = nullptr;
                }
            }

        private:
            Uint32 _size = 0;
            ArrayType* _array = nullptr;
        };

        template<class ArrayType>
        class Array2D_Allocator : public IArrayAllocator
        {
        public:
            Array2D_Allocator() { }
            Array2D_Allocator(Uint32 rows, Uint32 columns) : _rows(rows), _columns(columns)
            {
                free();
                allocate();
            }

            Array2D_Allocator(const Array2D_Allocator& arr)
            {
                _rows = arr.GetRows();
                _columns = arr.GetColumns();

                free();
                allocate();

                ArrayType** pArr = arr.Get();
                for (Uint32 i = 0; i < _rows; ++i)
                {
                    for (Uint32 j = 0; j < _columns; ++j)
                        _array[i][j] = pArr[i][j];
                }
            }

            ~Array2D_Allocator()
            {
                free();
            }

            ArrayType** Get() const
            {
                return _array;
            }

            Uint32 GetRows() const
            {
                return _rows;
            }

            Uint32 GetColumns() const
            {
                return _columns;
            }

            void CopyData(ArrayType* arr, Uint32 size)
            {
                if ((_rows * _columns) == size)
                {
                    for (Uint32 i = 0; i < _rows; ++i)
                    {
                        for (Uint32 j = 0; j < _columns; ++j)
                            _array[i][j] = *(arr + i * _columns + j);
                    }
                }
            }

            inline Array2D_Allocator<ArrayType>& Array2D_Allocator::operator=(const Array2D_Allocator<ArrayType> &arr)
            {
                _rows = arr.GetRows();
                _columns = arr.GetColumns();
                
                free();
                allocate();

                ArrayType** pArr = arr.Get();
                for (Uint32 i = 0; i < _rows; ++i)
                {
                    for (Uint32 j = 0; j < _columns; ++j)
                        _array[i][j] = pArr[i][j];
                }

                return *this;
            }

        private:
            void allocate() override
            {
                if (_rows != 0 && _columns != 0)
                {
                    _array = new ArrayType*[_rows];
                    for (Uint32 i = 0; i < _rows; ++i)
                        _array[i] = new ArrayType[_columns];
                }
            }

            void free() override
            {
                if (_array != nullptr)
                {
                    for (Uint32 i = 0; i < _rows; ++i)
                        delete[] _array[i];

                    delete[] _array;

                    _array = nullptr;
                }
            }

        private:
            Uint32 _rows = 0;
            Uint32 _columns = 0;
            ArrayType** _array = nullptr;
        };

        template<class ArrayType>
        class Array3D_Allocator : public IArrayAllocator
        {
        public:
            Array3D_Allocator() { }
            Array3D_Allocator(Uint32 depth, Uint32 rows, Uint32 columns) : _depth(depth), _rows(rows), _columns(columns)
            {
                free();
                allocate();
            }

            Array3D_Allocator(const Array3D_Allocator& arr)
            {
                _depth = arr.GetDepth();
                _rows = arr.GetRows();
                _columns = arr.GetColumns();

                free();
                allocate();

                ArrayType*** pArr = arr.Get();
                for (Uint32 i = 0; i < _depth; ++i)
                {
                    for (Uint32 j = 0; j < _rows; ++j)
                    {
                        for(Uint32 k = 0; k < _columns; ++k)
                            _array[i][j][k] = pArr[i][j][k];
                    }
                }
            }

            ~Array3D_Allocator()
            {
                free();
            }

            ArrayType*** Get() const
            {
                return _array;
            }

            Uint32 GetDepth() const
            {
                return _depth;
            }

            Uint32 GetRows() const
            {
                return _rows;
            }

            Uint32 GetColumns() const
            {
                return _columns;
            }

            void CopyData(ArrayType* arr, Uint32 size)
            {
                if ((_depth * _rows * _columns) == size)
                {
                    for (Uint32 i = 0; i < _depth; ++i)
                    {
                        for (Uint32 j = 0; j < _rows; ++j)
                        {
                            for (Uint32 k = 0; k < _columns; ++k)
                                _array[i][j][k] = *(arr + i * rows * _columns + j * columns + k);
                        }
                    }
                }
            }

            inline Array3D_Allocator<ArrayType>& Array3D_Allocator::operator=(const Array3D_Allocator<ArrayType> &arr)
            {
                _depth = arr.GetDepth();
                _rows = arr.GetRows();
                _columns = arr.GetColumns();

                free();
                allocate();

                ArrayType*** pArr = arr.Get();
                for (Uint32 i = 0; i < _depth; ++i)
                {
                    for (Uint32 j = 0; j < _rows; ++j)
                    {
                        for (Uint32 k = 0; k < _columns; ++k)
                            _array[i][j][k] = pArr[i][j][k];
                    }
                }

                return *this;
            }

        private:
            void allocate() override
            {
                if (_depth != 0 && _rows != 0 && _columns != 0)
                {
                    _array = new ArrayType**[_depth];
                    for (Uint32 i = 0; i < _depth; ++i)
                    {
                        _array[i] = new ArrayType*[_rows];
                        for (Uint32 j = 0; j < _rows; ++j)
                            _array[i][j] = new ArrayType[_columns];
                    }
                }
            }

            void free() override
            {
                if (_array != nullptr)
                {
                    for (Uint32 i = 0; i < _depth; ++i)
                    {
                        for (Uint32 j = 0; j < _rows; ++j)
                            delete[] _array[i][j];

                        delete[] _array[i];
                    }

                    delete[] _array;

                    _array = nullptr;
                }
            }
        private:
            Uint32 _depth = 0;
            Uint32 _rows = 0;
            Uint32 _columns = 0;
            ArrayType*** _array = nullptr;
        };
    }
}

#endif // !ARRAY_H

