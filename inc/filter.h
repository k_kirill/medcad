#pragma once

#ifndef FILTER_H
#define FILTER_H

#include "main.h"
#include "ArrayAllocator.h"
#include "DICOMImageCollection.h"

using namespace std;

using MedBox::Utils::Array1D_Allocator;
using MedBox::Utils::Array2D_Allocator;
using MedBox::Utils::Array3D_Allocator;

namespace MedBox
{
	namespace Imaging
	{
		class Filter
		{
		public:
			//This function apply Gaussian filter for input image 
			static void GaussianFilter(const DICOMImageCollection*, short, float);

			//This function apply median filter for input image
			static void MedianFilter(const DICOMImageCollection*, short);

			//This function apply mean filter for input image
			static void MeanFilter(const DICOMImageCollection*, short);

			//This function apply erosion filter for input image
			static void ErosionFilter(const DICOMImageCollection*, short);

			//This function apply dilation filter for input image
			static void DilationFilter(const DICOMImageCollection*, short);

			//This function apply open operation for input image
			static void OpenFunction(const DICOMImageCollection*, short);

			//This function apply close operation for input image
			static void CloseFunction(const DICOMImageCollection*, short);

			//This function apply close and open operation for input image successively
			static void OpenCloseFunction(const DICOMImageCollection*, short);

			//This function apply open and close operation for input image successively
			static void CloseOpenFunction(const DICOMImageCollection*, short);

		private:
			//This finction build convolution matrix
			static Array2D_Allocator<Float32> getGaussianKernel(short, float);

			//This function check size of matrix convolution
			static bool kernelSizeIsValid(short);

			//This function fill window
			static Array3D_Allocator<Sint16> makeExtendedImages(const DICOMImageCollection*, short);
            static void freeExtendedImages(short***, short, short, short);

			inline static short getMedianValue(short buffer[], short buffer_size, short b, short e)
			{
				short l = b, r = e;
				short piv = buffer[(l + r) / 2];
				while (l <= r)
				{
					while (buffer[l] < piv)
						l++;
					while (buffer[r] > piv)
						r--;
					if (l <= r)
						swap(buffer[l++], buffer[r--]);
				}
				if (b < r)
					getMedianValue(buffer, buffer_size, b, r);
				if (e > l)
					getMedianValue(buffer, buffer_size, l, e);

				return buffer[buffer_size / 2];
			} 

			inline static short getMinValue(short buffer[], short buffer_size)
			{
				short min = buffer[0];

				for (short i = 0; i < buffer_size; ++i)
					if (buffer[i] < min)
						min = buffer[i];

				return min;
			}

			inline static short getMaxValue(short buffer[], short buffer_size)
			{
				short max = buffer[0];

				for (short i = 0; i < buffer_size; ++i)
					if (buffer[i] > max)
						max = buffer[i];

				return max;
			}
		};
	}
}
#endif //FILTER_H
