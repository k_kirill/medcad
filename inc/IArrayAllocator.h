#pragma once

#ifndef IARRAY_ALLOCATOR_H
#define IARRAY_ALLOCATOR_H

namespace MedBox
{
    namespace Utils
    {
        class IArrayAllocator
        {
        public:
            virtual ~IArrayAllocator() { }
            virtual void allocate() = 0;
            virtual void free() = 0;
        };
    }
}

#endif // !IARRAY_ALLOCATOR_H

