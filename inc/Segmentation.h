#pragma once

#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include "main.h"

#include "DICOMImageCollection.h"

using MedBox::Imaging::DICOMImageCollection;

namespace MedBox
{
	namespace Imaging 
	{
        class DICOMImageCollection;
		//TODO:
		class Segmentation
		{
		public:
            static const void ThresholdSegmentation(DICOMImageCollection* collection, short t);
            static const void SplitMergeSegmentation(DICOMImageCollection* collection);
            static const void WatershedSegmentation(DICOMImageCollection* collection);
            static const void RegionGrowingSegmentation(DICOMImageCollection* collection);
		};
	}
}
#endif //SEGMENTATION_H
