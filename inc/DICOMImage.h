#pragma once

#ifndef DICOMImage_H
#define DICOMImage_H

#include "main.h"
#include "ObjectType.h"
#include "ArrayAllocator.h"

//DCMTK lib
#include <dcmtk\dcmdata\dcpxitem.h>
#include <dcmtk\dcmdata\dctk.h>
#include <dcmtk\dcmimage\diregist.h>
#include <dcmtk\dcmimgle\dcmimage.h>
#include <dcmtk\dcmjpeg\djdecode.h>
#include <dcmtk\dcmjpls\djdecode.h>

using MedBox::OBJECT_TYPE;
using MedBox::Utils::Array2D_Allocator;

namespace MedBox
{
	namespace Imaging
	{
		class DICOMImage
		{
		public:
            DICOMImage();
			DICOMImage(const char*);
            DICOMImage(const DICOMImage& image);

			~DICOMImage();

		public:
			//getters
			const Uint16 GetRows() const;
			const Uint16 GetColumns() const;
			const OBJECT_TYPE GetType() const;
            const string GetPatientName() const;
            const string GetPatientID() const;
            const string GetPath() const;
            Sint16** GetData() const;

            const Float64 GetX_PixSP() const;
            const Float64 GetY_PixSP() const;
            const Float64 GetSliceLoc() const;

            const Array2D_Allocator<Sint16>& GetArray() const;

            inline DICOMImage& DICOMImage::operator=(const DICOMImage &image)
            {
                _rows = image.GetRows();
                _columns = image.GetColumns();
                _patient_name = image.GetPatientName();
                _patient_id = image.GetPatientID();
                _x_pix_sp = image.GetX_PixSP();
                _y_pix_sp = image.GetY_PixSP();
                _slice_loc = image.GetSliceLoc();
                _path = image.GetPath();

                _array = image.GetArray();

                return *this;
            }

        private:
            bool parseData();
            const void extractPixelValues(const DicomImage&);
            const void allocate();

		private:
			OBJECT_TYPE _type = OBJECT_TYPE::IMAGE_TYPE;
			Uint16 _rows = 0;
			Uint16 _columns = 0;
            Array2D_Allocator<Sint16> _array;
            string _patient_name = string();
            string _patient_id = string();
            Float64 _x_pix_sp = 0.0f;
            Float64 _y_pix_sp = 0.0f;
            Float64 _slice_loc = 0.0f;

            string _path = string();
		};
	}
}
#endif //DICOMImage_H

