#pragma once

#ifndef EXPORTER_H
#define EXPORTER_H

#include "DICOMImage.h"
#include "TriMesh.h"
#include "VCGTypes.h"
#include "Format.h"
#include "MeshConverter.h"

#include "main.h"

//DCMTK lib
#include <dcmtk\dcmdata\dcpxitem.h>
#include <dcmtk\dcmdata\dctk.h>
#include <dcmtk\dcmimage\diregist.h>
#include <dcmtk\dcmimgle\dcmimage.h>
#include <dcmtk\dcmjpeg\djdecode.h>
#include <dcmtk\dcmjpls\djdecode.h>

//VCG lib
#include <wrap/io_trimesh/export_stl.h>
#include <wrap/io_trimesh/export_ply.h>
#include <wrap/io_trimesh/export_off.h>
#include <wrap/io_trimesh/export_obj.h>
#include <wrap/io_trimesh/io_mask.h>

using namespace vcg;
using namespace VCGTypes;

using MedBox::Modeling::MeshConverter;
using MedBox::Imaging::DICOMImage;

namespace MedBox 
{
	namespace IO
	{
		template<class Obj>
		class Exporter
		{
		public:
			//Export to STL
			static bool exportToSTL(Obj* obj, const char* path, const bool binary = true)
			{
				if (save(obj, path, FORMAT::STL, binary)) return true;

				else { cout << "Error happened while writing:" << path << "\n"; return false; }
			}

			//Export to PLY
			static bool exportToPLY(Obj* obj, const char* path, const bool binary = true)
			{
				if (save(obj, path, FORMAT::PLY, binary)) return true;

				else { cout << "Error happened while writing:" << path << "\n"; return false; }
			}

			//Export to OFF
			static bool exportToOFF(Obj* obj, const char* path)
			{
				if (save(obj, path, FORMAT::OFF)) return true;

				else { cout << "Error happened while writing:" << path << "\n"; return false; }
			}

			//Export to OBJ
			static bool exportToOBJ(Obj* obj, const char* path)
			{
				if (save(obj, path, FORMAT::OBJ)) return true;

				else { cout << "Error happened while writing:" << path << "\n"; return false; }
			}

			//TODO:
			static bool exportToDICOM(Obj* obj, const char* path)
			{
                if (obj == NULL)
                {
                    std::cerr << "Error: the image object is empty!\n";
                    return false;
                }

                if (obj->GetType() != OBJECT_TYPE::IMAGE_TYPE)
                {
                    std::cerr << "Error: the type is not allowed!\n";
                    return false;
                }

                cout << "Writing to DICOM file:" << path << "\n";

                DcmFileFormat fileformat;
                OFCondition status = fileformat.loadFile(obj->GetPath().c_str());

                if (status.good())
                {
                    DcmDataset *dataset = fileformat.getDataset();

                    short** data = obj->GetData();
                    const Uint16 rows = obj->GetRows();
                    const Uint16 columns = obj->GetColumns();
                    Uint32 size = rows * columns;

                    const Uint16* _data = convertMultiToSingleArray(data, rows, columns);

                    dataset->putAndInsertUint16Array(DCM_PixelData, _data, size);
                    OFCondition _status = fileformat.saveFile(path, EXS_LittleEndianExplicit);

                    if (_status.bad())
                    {
                        cerr << "Error: cannot write DICOM file (" << _status.text() << ")" << endl;
                        return false;
                    }
                    delete[] _data;
                }
                return true;
			}

			//Export to bin file
			static bool exportToBIN(Obj* obj, const char* path)
			{
				if (obj == NULL)
				{
					cout << "Error: the image object is empty!\n";
					return false;
				}

				if (obj->GetType() != OBJECT_TYPE::IMAGE_TYPE)
				{
					std::cout << "Error: the type is not allowed!\n";
					return false;
				}

                try
                {
                    cout << "Writing to binary file:" << path << "\n";

                    Uint16 rows = obj->GetRows();
                    short** data = obj->GetData();

                    std::ofstream file(path, ios_base::binary | ios_base::trunc);
                    for (Sint32 i = 0; i < rows; ++i)
                    {
                        const char* pointer = reinterpret_cast<const char*>(&data[i][0]);
                        file.write(pointer, sizeof(data[i][0]) * rows);
                    }
                    file.close();
                    cout << "Successful writing!\n";
                    return true;
                }

                catch (...) { cerr << "Error: cannot save DICOM file\n"; return false; }
			}

		private:
			//Save function
			 static bool save(Obj* obj, const char* path, FORMAT format, const bool binary = true)
			 {
				 if (obj == NULL)
				 {
					 cout << "Error: the mesh object is empty!\n";
					 return false;
				 }

				 if (obj->GetType() != OBJECT_TYPE::TRI_MESH_TYPE)
				 {
					 std::cout << "Error: the type is not allowed!\n";
					 return false;
				 }

				 cout << "Exporting...\n";

				 //Convert here
				 VCGMesh mesh;
				 if (!MeshConverter<Obj>::ConvertToVCGTriMesh(obj, mesh))
					 return false;

				 switch (format)
				 {
				 case FORMAT::STL:
				 {
					 if (binary) tri::io::ExporterSTL<VCGMesh>::Save(mesh, path, true);
					 else tri::io::ExporterSTL<VCGMesh>::Save(mesh, path, false);
				 } break;

				 case FORMAT::PLY:
				 {
					 if (binary) tri::io::ExporterPLY<VCGMesh>::Save(mesh, path, true);
					 else tri::io::ExporterPLY<VCGMesh>::Save(mesh, path, false);
				 } break;

				 case FORMAT::OFF:
				 {
					 tri::io::ExporterOFF<VCGMesh>::Save(mesh, path);
				 } break;

				 case FORMAT::OBJ:
				 {
					 int mask = io::Mask::IOM_NONE;
					 tri::io::ExporterOBJ<VCGMesh>::Save(mesh, path, mask);
				 } break;
				 }

				 cout << "Successful writing!\n";
				 return true;
			 }

             const static Uint16* convertMultiToSingleArray(short** init_arr, Uint16 rows, Uint16 columns)
             {
                 Uint16* arr = new Uint16[rows*columns];

                 for (Sint32 i = 0; i < rows; ++i)
                 {
                     for (Sint32 j = 0; j < columns; ++j)
                         *(arr + i * columns + j) = (Uint16)init_arr[i][j];
                 }
                 return arr;
             }
		};
	}
}
#endif //EXPORTER_H