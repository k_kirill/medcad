#pragma once

#ifndef MAIN_H
#define MAIN_H

//C++ std lib
#include <memory>
#include <string>
#include <conio.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <set>
#include <list>
#include <array>
#include <algorithm>
#include <map>
#include <cmath>
#include <Windows.h>
#include <thread>
#include <mutex>
#include <functional>
#include <stdexcept>
#include <climits>

#include <dcmtk/ofstd/oftypes.h>

using std::shared_ptr;
using std::unique_ptr;
using std::greater;
using std::list;
using std::ofstream;
using std::ios_base;
using std::string;
using std::cout;
using std::cerr;
using std::vector;
using std::array;
using std::thread;
using std::set;
using std::mutex;

#endif // !MAIN_H

