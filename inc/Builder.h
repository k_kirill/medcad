#pragma once

#ifndef BUILDER_H
#define BUILDER_H

#include "GPrimitives.h"
#include "LookUpTable.h"
#include "GridCell.h"
#include "DICOMImageCollection.h"
#include "SegmentationType.h"
#include "main.h"

using MedBox::Imaging::SEGMENTATION_TYPE;
using MedBox::Modeling::GridCell;
using MedBox::Imaging::DICOMImageCollection;
using MedBox::Imaging::DICOMImage;

using Tri = MedBox::Modeling::Geometry::Triangle;
using Vert = MedBox::Modeling::Geometry::Vertex;


namespace MedBox
{
	namespace Modeling
	{
		class Builder sealed {

		public:
			Builder(DICOMImageCollection*, bool, const short size = 1, const short iso_surface = (std::numeric_limits<short>::min)());

		public:
			bool Build(Uint16 i, Uint16 j, Uint32 k, const Uint32 offset = 0);
			list<Tri> getTriangles();
			
		private:
			short _iso_surface;
			bool _standartMC;
			GridCell _cell;
            DICOMImageCollection* _collection;
            SEGMENTATION_TYPE _segm_type;
			short _case, _config, _subconfig, _cell_size;

		private:
			Vert getIntersection(short);
			void getVertices(short, short *);
			short getNodeCaseNumber();
			void addTriangles(list<Tri> &, const __int8[], short);
			bool testFace(__int8);
			bool testInterior(__int8);

			inline void addAdditionalVertex()
			{
				_cell.additional_vertex = Vert(0, 0, 0);
				short count_ = 0;

				for (short i = 0; i < 8; ++i)
				{
					_cell.additional_vertex += _cell.vertex[i];
					count_++;
				}
				_cell.additional_vertex = _cell.additional_vertex / static_cast<float>(count_);
			}
		};
	}
}
#endif //BUILDER_H
