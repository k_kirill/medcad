#pragma once

#ifndef SMOOTHER_H
#define SMOOTHER_H

#include "main.h"
#include "ArrayAllocator.h"
#include "GPrimitives.h"
#include "TriMesh.h"

using MedBox::Modeling::TriMesh;

namespace MedBox
{
	namespace Modeling
	{
		class TriMeshSmoothing
		{
		public:
			const static void TaubinSmooth(TriMesh*                    , 
                                           const Float32 lambda = 0.55f, 
                                           const Float32 mu = -0.6f    , 
                                           const Uint16 iterations = 10);

		private:
			const static void laplaceSmooth(TriMesh*, const float scale);
		};
	}
}

#endif //SMOOTHER_H