#pragma once

#ifndef MARCHINGCUBE_H
#define MARCHINGCUBE_H

#include "main.h"
#include "Builder.h"
#include "DICOMImageCollection.h"
#include "TriMesh.h"

#include <boost/thread/scoped_thread.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using MedBox::Imaging::DICOMImageCollection;

namespace MedBox
{
	namespace Modeling
	{
		class MeshGenerator 
		{
		public:
			static TriMesh* BuildModel(DICOMImageCollection* collection               ,
                                       const bool standartMC = false                  ,
                                       const GridCell::Size size = GridCell::Size::One,
                                       const short iso_surface = (std::numeric_limits<short>::min)());

            static TriMesh* BuildModel(const vector<DICOMImageCollection*>& collections,
                                       const bool standartMC = false                   ,
                                       const GridCell::Size size = GridCell::Size::One ,
                                       const short iso_surface = (std::numeric_limits<short>::min)());

            static void generateTriangles(list<Tri>& triangles, DICOMImageCollection* collection, bool standartMC, Uint8 cell_size, short iso_surface);
		};
	}
}

#endif //MARCHINGCUBE_H
