#pragma once

#ifndef DECIMATOR_H
#define DECIMATOR_H

#include "main.h"
#include "TriMesh.h"

#include "VCGTypes.h"

namespace M = MedBox::Modeling;
using namespace VCGTypes;

namespace MedBox
{
	namespace Modeling
	{
		class TriMeshSimplification
		{
		public:
            const static void Simplify(M::TriMesh*                                                    ,
                                       const float target_error = (std::numeric_limits<double>::max)(),
                                       float reduce_fraction = 0.5f                                   ,
                                       const bool cleaning_flag = false);

            //Setters
            const static void SetBoundaryWeight(bool);
            const static void SetCosineThr(Float64);
            const static void SetFastPreserveBoundary(bool);
            const static void SetNormalCheck(bool);
            const static void SetNormalThrRad(Float64);
            const static void SetOptimalPlacement(bool);
            const static void SetPreserveBoundary(bool);
            const static void SetPreserveTopology(bool);
            const static void SetQuadricEpsilon(Float64);
            const static void SetQualityCheck(bool);
            const static void SetQualityQuadric(bool);
            const static void SetQualityThr(Float64);
            const static void SetQualityWeight(bool);
            const static void SetQualityWeightFactor(Float64);
            const static void SetScaleFactor(Float64);
            const static void SetScaleIndependent(bool);
            const static void SetUseArea(bool);
            const static void SetUseVertexWeight(bool);


		private:
			static TriEdgeCollapseQuadricParameter parameters;
		};
	}
}

#endif // !DECIMATOR_H

