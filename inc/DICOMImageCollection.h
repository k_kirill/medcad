#pragma once

#ifndef DICOMImageCollection_H
#define DICOMImageCollection_H

#include "main.h"
#include "DICOMImage.h"
#include "SegmentationType.h"
#include "ObjectType.h"

using MedBox::Imaging::SEGMENTATION_TYPE;
using MedBox::OBJECT_TYPE;

namespace MedBox
{
	namespace Imaging
	{
		class DICOMImageCollection
		{
		public:
            DICOMImageCollection() = default;
            DICOMImageCollection(Uint32, const Uint16 offset = 0);
            DICOMImageCollection(DICOMImage* images);
			~DICOMImageCollection();

		public:
			const DICOMImage* GetImages() const;
            const DICOMImage& First() const;
            const DICOMImage& Last() const;
            const DICOMImage& Image(Uint32) const;
			const Uint32 GetCount() const;
            const Uint32 GetSize() const;
            const Uint16 GetOffset() const;
			const void Add(const DICOMImage&);
            bool IsEmpty() const;
            bool IsFull() const;
            OBJECT_TYPE GetType() const;

            static vector<DICOMImageCollection*> Split(DICOMImageCollection*, Uint32);
            static DICOMImageCollection* Merge(const vector<DICOMImageCollection*>&);

		public:
			SEGMENTATION_TYPE SegmentationMark = SEGMENTATION_TYPE::NONE;

		private:
			DICOMImage* _images;
            Uint32 _count = 0;
            Uint32 _size = 0;
            Uint16 _offset = 0;
            OBJECT_TYPE _type = DICOM_IMG_COLL_TYPE;
		};
	}
}
#endif //DICOMImageCollection_H
