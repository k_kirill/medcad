#pragma once

#ifndef SEGMENTATION_TYPE_H
#define SEGMENTATION_TYPE_H

namespace MedBox
{
    namespace Imaging
    {
        typedef enum
        {
            NONE,
            THRESHOLD,
            WATERSHED,
            REGION_GROWING
        } SEGMENTATION_TYPE;
    }
}

#endif // !SEGMENTATION_TYPE_H