#pragma once

#ifndef FORMAT_H
#define FORMAT_H

namespace MedBox
{
	namespace IO
	{
		typedef enum
		{
			STL,
			PLY,
			OFF,
			OBJ,
			BIN,
			DICOM
		} FORMAT;
	}
}

#endif // !FORMAT_H

