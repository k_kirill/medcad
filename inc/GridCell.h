#pragma once

#ifndef GRIDCELL_H
#define GRIDCELL_H

#include "GPrimitives.h"

using Vert = MedBox::Modeling::Geometry::Vertex;

namespace MedBox 
{
	namespace Modeling 
	{
		class GridCell
		{
		public:
			typedef enum
			{
				One = 1,
				Two = 2,
				Four = 4,
				Eight = 8
			} Size;

			Vert additional_vertex;
			Vert vertex[8];
			short value[8];
			bool nodeParity[8];

		public:
			GridCell() { }
			GridCell(Vert vertex[8], short value[8])
			{
				for (short i = 0; i < 8; ++i)
				{
					this->vertex[i] = vertex[i];
					this->value[i] = value[i];
				}
			}
		};
	}
}
#endif //GRIDCELL_H