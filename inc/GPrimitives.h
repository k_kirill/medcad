#pragma once
#ifndef GPRIMITIVES_H
#define GPRIMITIVES_H

#include "main.h"

namespace MedBox
{
    namespace Modeling
    {
        namespace Geometry
        {
            ///Vertex struct
            struct Vertex
            {
            public:
                Float64 x = 0.0, y = 0.0, z = 0.0;
                Float64 r = 0;
                int index = -1;

            public:
                Vertex() { }
                Vertex(Float64 x, Float64 y, Float64 z) : x(x), y(y), z(z) { r = length(); }
                Vertex(Float64 coords[]) : x(coords[0]), y(coords[1]), z(coords[2]) { r = length(); }

            public:
                //Inline operators
                inline Vertex Vertex::operator-(const Vertex &right) const
                {
                    Vertex v;

                    v.x = x - right.x;
                    v.y = y - right.y;
                    v.z = z - right.z;

                    return v;
                }

                inline Vertex Vertex::operator+(const Vertex &right) const
                {
                    Vertex v;

                    v.x = x + right.x;
                    v.y = y + right.y;
                    v.z = z + right.z;

                    return v;
                }

                inline Vertex& Vertex::operator+=(const Vertex &right)
                {
                    x += right.x;
                    y += right.y;
                    z += right.z;

                    return *this;
                }

                inline Vertex& Vertex::operator-=(const Vertex &right)
                {
                    x -= right.x;
                    y -= right.y;
                    z -= right.z;

                    return *this;
                }

                inline Vertex Vertex::operator*(const Float64 &right) const
                {
                    Vertex v;

                    v.x = x * right;
                    v.y = y * right;
                    v.z = z * right;

                    return v;
                }

                inline Vertex Vertex::operator/(const Float32 &right) const
                {
                    Vertex v;

                    v.x = x / right;
                    v.y = y / right;
                    v.z = z / right;

                    return v;
                }

                inline Vertex& Vertex::operator=(const Vertex &right)
                {
                    x = right.x;
                    y = right.y;
                    z = right.z;
                    r = right.r;
                    index = right.index;

                    return *this;
                }

                inline bool Vertex::operator<(const Vertex &right) const
                {
                    if (right.x > x)
                        return true;
                    else if (right.x < x)
                        return false;

                    if (right.y > y)
                        return true;
                    else if (right.y < y)
                        return false;

                    if (right.z > z)
                        return true;
                    else if (right.z < z)
                        return false;

                    return false;
                }

                inline bool Vertex::operator==(const Vertex &right) const
                {
                    if (std::abs(right.x - x) < 0.00001 &&
                        std::abs(right.y - y) < 0.00001 && 
                        std::abs(right.z - z) < 0.00001)
                        return true;

                    return false;
                }

                inline Vertex Vertex::cross(const Vertex &right) const
                {
                    Vertex v;

                    v.x = y * right.z - z * right.y;
                    v.y = z * right.x - x * right.z;
                    v.z = x * right.y - y * right.x;

                    return v;
                }

                inline Float64 Vertex::distance_sq(const Vertex &right) const
                {
                    return (right.x - x)*(right.x - x) + (right.y - y)*(right.y - y) + (right.z - z)*(right.z - z);
                }

            private:
                inline Float64 Vertex::length() const
                {
                    Float64 lenght;
                    lenght = sqrt(x*x + y * y + z * z);

                    return lenght;
                }
            };

            ///Vector struct
            struct Vector
            {
            public:
                Float64 Nx = 0.0, Ny = 0.0, Nz = 0.0;

            public:
                Vector() { }
                Vector(Float64 values[]) : Nx(values[0]), Ny(values[1]), Nz(values[2])
                {
                    _length = sqrt((Nx * Nx) + (Ny * Ny) + (Nz * Nz));
                }
                Vector(Float64 Nx, Float64 Ny, Float64 Nz) : Nx(Nx), Ny(Ny), Nz(Nz)
                {
                    _length = sqrt((Nx * Nx) + (Ny * Ny) + (Nz * Nz));
                };

                Vector(Vertex v[])
                {
                    this->Nx = (v[1].y - v[0].y) * (v[2].z - v[0].z) - (v[1].z - v[0].z) * (v[2].y - v[0].y);
                    this->Ny = (v[1].z - v[0].z) * (v[2].x - v[0].x) - (v[1].x - v[0].x) * (v[2].z - v[0].z);
                    this->Nz = (v[1].x - v[0].x) * (v[2].y - v[0].y) - (v[1].y - v[0].y) * (v[2].x - v[0].x);
                }

            public:
                inline void Vector::Normalize()
                {
                    _length = Length();

                    if (_length != 0.0)
                    {
                        this->Nx = this->Nx / _length;
                        this->Ny = this->Ny / _length;
                        this->Nz = this->Nz / _length;
                    }
                }

                inline Float64 Vector::Length()
                {
                    return _length;
                }

                //Inline operators--------------------------------
                inline Vector& operator=(const Vertex &right)
                {
                    this->Nx = right.x;
                    this->Ny = right.y;
                    this->Nz = right.z;

                    _length = sqrt((Nx * Nx) + (Ny * Ny) + (Nz * Nz));

                    return *this;
                }

                inline Vector operator+(const Vector &right) const
                {
                    Vector n;

                    n.Nx = Nx + right.Nx;
                    n.Ny = Ny + right.Ny;
                    n.Nz = Nz + right.Nz;

                    return n;
                }

                inline Vector& operator=(const Vector &right)
                {
                    this->Nx = right.Nx;
                    this->Ny = right.Ny;
                    this->Nz = right.Nz;

                    _length = sqrt((Nx * Nx) + (Ny * Ny) + (Nz * Nz));

                    return *this;
                }

                inline Vector Vector::cross(const Vector &right) const
                {
                    Vector v;

                    v.Nx = (Ny * right.Nz - right.Ny * Nz);
                    v.Ny = (Nz * right.Nx - right.Nz * Nx);
                    v.Nz = (Nx * right.Ny - right.Nx * Ny);

                    return v;
                }

            private:
                Float64 _length;
            };
            ///

            ///Edge struct
            struct Edge
            {
            public:
                unsigned int vertex_indices[2];
                int index = -1;

            public:
                Edge() { }
                Edge(unsigned int a, unsigned int b)
                {
                    if (a < b)
                    {
                        vertex_indices[0] = a;
                        vertex_indices[1] = b;
                    }

                    else
                    {
                        vertex_indices[0] = b;
                        vertex_indices[1] = a;
                    }
                }

                inline bool Edge::operator==(const Edge &right)
                {
                    if ((vertex_indices[0] == right.vertex_indices[0] &&
                        vertex_indices[1] == right.vertex_indices[1]))
                        return true;

                    else
                        return false;
                }

                inline bool Edge::operator<(const Edge &right) const
                {
                    if (right.vertex_indices[0] > vertex_indices[0])
                        return true;
                    else if (right.vertex_indices[0] < vertex_indices[0])
                        return false;

                    if (right.vertex_indices[1] > vertex_indices[1])
                        return true;
                    else if (right.vertex_indices[1] < vertex_indices[1])
                        return false;

                    return false;
                }
            };
            ///

            ///LineSegment struct
            struct LineSegment
            {
            public:
                LineSegment(Vertex a, Vertex b)
                {
                    vertices[0] = a;
                    vertices[1] = b;
                    vector = vertices[1] - vertices[0];
                }

            public:
                inline Vertex* LineSegment::GetVertices()
                {
                    return vertices;
                }

                inline Vector LineSegment::GetVector()
                {
                    return vector;
                }

                inline bool LineSegment::IsIntersection(const LineSegment &line) const
                {
                    //From initial point of the first line to second line points
                    LineSegment lines1[] = { LineSegment(vertices[0], line.vertices[0]),
                                             LineSegment(vertices[0], line.vertices[1]) };

                    //From initial point of the second line to first line points
                    LineSegment lines2[] = { LineSegment(line.vertices[0], vertices[0]),
                                             LineSegment(line.vertices[0], vertices[1]) };

                    //Single component not equal zero only
                    Float64 result1[2] = { (vector.Ny * lines1[0].vector.Nz - vector.Nz * lines1[0].vector.Ny) +
                                           (vector.Nz * lines1[0].vector.Nx - vector.Nx * lines1[0].vector.Nz) +
                                           (vector.Nx * lines1[0].vector.Ny - vector.Ny * lines1[0].vector.Nx),

                                           (vector.Ny * lines1[1].vector.Nz - vector.Nz * lines1[1].vector.Ny) +
                                           (vector.Nz * lines1[1].vector.Nx - vector.Nx * lines1[1].vector.Nz) +
                                           (vector.Nx * lines1[1].vector.Ny - vector.Ny * lines1[1].vector.Nx) };

                    Float64 result2[2] = { (line.vector.Ny * lines2[0].vector.Nz - line.vector.Nz * lines2[0].vector.Ny) +
                                           (line.vector.Nz * lines2[0].vector.Nx - line.vector.Nx * lines2[0].vector.Nz) +
                                           (line.vector.Nx * lines2[0].vector.Ny - line.vector.Ny * lines2[0].vector.Nx),

                                           (line.vector.Ny * lines2[1].vector.Nz - line.vector.Nz * lines2[1].vector.Ny) +
                                           (line.vector.Nz * lines2[1].vector.Nx - line.vector.Nx * lines2[1].vector.Nz) +
                                           (line.vector.Nx * lines2[1].vector.Ny - line.vector.Ny * lines2[1].vector.Nx) };

                    bool flags[2] = { false, false };

                    if ((result1[0] > 0 && result1[1] < 0) || (result1[0] < 0 && result1[1] > 0))
                        flags[0] = true;

                    if ((result2[0] > 0 && result2[1] < 0) || (result2[0] < 0 && result2[1] > 0))
                        flags[1] = true;

                    if (flags[0] && flags[1])
                        return true;

                    return false;
                }

                inline LineSegment& LineSegment::ProjectToXY()
                {
                    vertices[0].z = 0.0f;
                    vertices[1].z = 0.0f;
                    vector = vertices[1] - vertices[0];

                    return *this;
                }

                inline LineSegment& LineSegment::ProjectToXZ()
                {
                    vertices[0].y = 0.0f;
                    vertices[1].y = 0.0f;
                    vector = vertices[1] - vertices[0];

                    return *this;
                }

                inline LineSegment& LineSegment::ProjectToYZ()
                {
                    vertices[0].x = 0.0f;
                    vertices[1].x = 0.0f;
                    vector = vertices[1] - vertices[0];

                    return *this;
                }

            private:
                Vertex vertices[2];
                Vertex center;
                Vector vector;
                Float64 length;
            };

            ///Face struct
            struct Face
            {
            public:
                Float64 A, B, C, D;
                Vector normal;

            public:
                Face() { }
                Face(Vertex vertex, Vector normal)
                {
                    this->A, this->normal.Nx = normal.Nx;
                    this->B, this->normal.Ny = normal.Ny;
                    this->C, this->normal.Nz = normal.Nz;

                    this->normal.Normalize();

                    Float64 x = vertex.x;
                    Float64 y = vertex.y;
                    Float64 z = vertex.z;

                    this->D = -(A * x + B * y + C * z);
                }

                Face(Vertex v[])
                {
                    this->A = (v[1].y - v[0].y) * (v[2].z - v[0].z) - (v[2].y - v[0].y) * (v[1].z - v[0].z);
                    this->B = (v[1].z - v[0].z) * (v[2].x - v[0].x) - (v[2].z - v[0].z) * (v[1].x - v[0].x);
                    this->C = (v[1].x - v[0].x) * (v[2].y - v[0].y) - (v[2].x - v[0].x) * (v[1].y - v[0].y);
                    this->D = -(A * v[0].x + B * v[0].y + C * v[0].z);

                    this->normal.Nx = A;
                    this->normal.Ny = B;
                    this->normal.Nz = C;
                    this->normal.Normalize();
                }

                Face(Vertex& v0, Vertex& v1, Vertex& v2)
                {
                    this->A = (v1.y - v0.y) * (v2.z - v0.z) - (v2.y - v0.y) * (v1.z - v0.z);
                    this->B = (v1.z - v0.z) * (v2.x - v0.x) - (v2.z - v0.z) * (v1.x - v0.x);
                    this->C = (v1.x - v0.x) * (v2.y - v0.y) - (v2.x - v0.x) * (v1.y - v0.y);
                    this->D = -(A*v0.x + B * v0.y + C * v0.z);

                    this->normal.Nx = A;
                    this->normal.Ny = B;
                    this->normal.Nz = C;
                    this->normal.Normalize();
                }

            public:
                inline bool Face::IsLies(const Vertex &vertex) const
                {
                    Float64 distance = GetDistance(vertex);

                    if (distance < 0.0001)
                        return true;

                    return false;
                }

                inline Float64 Face::GetDistance(const Vertex &vertex) const
                {
                    Float64 distance = A * vertex.x + B * vertex.y + C * vertex.z + D;

                    return distance;
                }
            };
            ///

            ///Triangle struct
            struct Triangle : public Face
            {
            public:
                Vertex v[3];

                Float64 quality;
                Float64 area;
                Float64 p;

            public:
                Triangle() : Face()
                { }

                Triangle(Vertex v[]) : Face(v)
                {
                    this->v[0] = v[0];
                    this->v[1] = v[1];
                    this->v[2] = v[2];

                    set_normal();
                    set_parameters();
                }

                Triangle(Vertex &v1, Vertex &v2, Vertex &v3) : Face(v1, v2, v3)
                {
                    this->v[0] = v1;
                    this->v[1] = v2;
                    this->v[2] = v3;

                    set_normal();
                    set_parameters();
                }

            public:
                inline void Triangle::RecalculateParameters()
                {
                    set_parameters();
                }

                inline void Triangle::RecalculateTriangleNormal()
                {
                    set_normal();
                }

                inline bool Triangle::IsIntersection(const Triangle &triangle) const
                {
                    Face p1(this->v[0], this->normal);
                    Face p2(triangle.v[0], triangle.normal);

                    //d are distances from first triangle to second triangle Face and vice versa
                    //d[i][j] is distance from Face i to j point second Face 
                    Float64 distances[2][3] = { { p1.GetDistance(triangle.v[0]), p1.GetDistance(triangle.v[1]), p1.GetDistance(triangle.v[2]) },
                                                { p2.GetDistance(this->v[0]), p2.GetDistance(this->v[1]), p2.GetDistance(this->v[2]) } };

                    //Determine IsLies vertices of first trinagle on the second triangle Face
                    if (distances[0][0] != 0 && distances[0][1] != 0 && distances[0][2] != 0)
                    {
                        //If every vertices heve same sign, then first triangle lies on one side of second triangle Face
                        //end operation
                        if ((distances[0][0] > 0 && distances[0][1] > 0 && distances[0][2] > 0) ||
                            (distances[0][0] < 0 && distances[0][1] < 0 && distances[0][2] < 0))
                            return false;
                    }

                    //This operations is same for second triangle
                    if (distances[1][0] != 0 && distances[1][1] != 0 && distances[1][2] != 0)
                    {
                        if ((distances[1][0] > 0 && distances[1][1] > 0 && distances[1][2] > 0) ||
                            (distances[1][0] < 0 && distances[1][1] < 0 && distances[1][2] < 0))
                            return false;
                    }

                    //Check co-planar triangles intersection
                    if (std::abs(p1.A - p2.A) < 0.0001 &&
                        std::abs(p1.B - p2.B) < 0.0001 &&
                        std::abs(p1.C - p2.C) < 0.0001 &&
                        std::abs(p1.D - p2.D) < 0.0001)
                    {
                        bool Face_intersection = coplanar_test_intersection(triangle);

                        if (Face_intersection)
                            return true;
                    }

                    //Intersection test in the 3 dimensional space
                    if (test_intersection(triangle, distances))
                        return true;

                    return false;
                }

                inline Triangle& operator=(const Triangle &t)
                {
                    this->v[0] = t.v[0];
                    this->v[1] = t.v[1];
                    this->v[2] = t.v[2];

                    this->normal = t.normal;
                    this->quality = t.quality;

                    return *this;
                }

                inline bool Triangle::operator==(const Triangle &t) const
                {
                    if ((t.v[0] == v[0] || t.v[0] == v[1] || t.v[0] == v[2]) &&
                        (t.v[1] == v[0] || t.v[1] == v[1] || t.v[1] == v[2]) &&
                        (t.v[2] == v[0] || t.v[2] == v[1] || t.v[2] == v[2]))
                        return true;

                    return false;
                }

                inline bool Triangle::ExistVertexByIndex(const unsigned int index) const
                {
                    for (short i = 0; i < 3; ++i)
                    {
                        if (v[i].index == index)
                            return true;
                    }
                    return false;
                }

            private:
                inline void Triangle::set_normal()
                {
                    normal = Vector(v);
                }

                inline void Triangle::set_parameters()
                {
                    LineSegment edges[3] = { LineSegment(v[0], v[1]),
                                             LineSegment(v[0], v[2]),
                                             LineSegment(v[1], v[2]) };

                    Float64 a = edges[0].GetVector().Length();
                    Float64 b = edges[1].GetVector().Length();
                    Float64 c = edges[2].GetVector().Length();

                    p = a + b + c;
                    Float64 p_ = p / 2.0;
                    area = sqrt(p_ * (p_ - a) * (p_ - b) * (p_ - c));

                    quality = 4 * sqrt(3) * area / (a * a + b * b + c * c);

                    if (isnan(quality))
                        quality = 0;
                }

                inline bool Triangle::coplanar_test_intersection(const Triangle& triangle) const
                {
                    Face p1(this->v[0], this->normal);

                    //Select the first Face
                    Vector n(p1.A, p1.B, p1.C);

                    Vector xy_n(0.0f, 0.0f, 1.0f);
                    Vector xz_n(0.0f, 1.0f, 0.0f);
                    Vector yz_n(1.0f, 0.0f, 0.0f);

                    //Determine cos value between Face normal and basic Faces
                    Float64 length = sqrt(n.Nx * n.Nx + n.Ny * n.Ny + n.Nz * n.Nz);
                    Float64 cos_xy = abs(n.Nz * xy_n.Nz);
                    Float64 cos_xz = abs(n.Ny * xz_n.Ny);
                    Float64 cos_yz = abs(n.Nx * yz_n.Nx);

                    //Find max cos value
                    Float64 max_cos = (std::max)({ cos_xy, cos_xz, cos_yz });

                    //For the first trinagle edges
                    LineSegment sides1[3] = { LineSegment(this->v[0], this->v[1]),
                                              LineSegment(this->v[0], this->v[2]),
                                              LineSegment(this->v[1], this->v[2]) };

                    //For the second trinagle edges
                    LineSegment sides2[3] = { LineSegment(triangle.v[0], triangle.v[1]),
                                              LineSegment(triangle.v[0], triangle.v[2]),
                                              LineSegment(triangle.v[1], triangle.v[2]) };

                    bool projected = false;

                    //Test on the XY Face lines intersection 
                    if (std::abs(max_cos - cos_xy) < 0.000001)
                    {
                        for (short i = 0; i < 3; ++i)
                        {
                            sides1[i].ProjectToXY();
                            sides2[i].ProjectToXY();
                        }
                        projected = true;
                    }

                    //Test on the XZ Face lines intersection 
                    if (std::abs(max_cos - cos_xz) < 0.000001 && !projected)
                    {
                        for (short i = 0; i < 3; ++i)
                        {
                            sides1[i].ProjectToXZ();
                            sides2[i].ProjectToXZ();
                        }
                        projected = true;
                    }

                    //Test on the YZ Face lines intersection 
                    if (std::abs(max_cos - cos_yz) < 0.000001 && !projected)
                    {
                        for (short i = 0; i < 3; ++i)
                        {
                            sides1[i].ProjectToYZ();
                            sides2[i].ProjectToYZ();
                        }
                        projected = true;
                    }

                    if (projected)
                    {
                        for (short i = 0; i < 3; ++i)
                        {
                            for (short j = 0; j < 3; ++j)
                            {
                                if (sides1[i].IsIntersection(sides2[j]))
                                    return true;
                            }
                        }
                    }

                    return false;
                }

                //TODO
                inline bool Triangle::test_intersection(const Triangle& triangle, const Float64(*distances)[3]) const
                {
                    Face p1(this->v[0], this->normal);
                    Face p2(triangle.v[0], triangle.normal);

                    //Detect normal vector for ortogonal Face to Face 1 and Face 2 
                    Vector n = p1.normal.cross(p2.normal);

                    //Using optimized intersection method
                    Float64 max_component = (std::max)({ n.Nx, n.Ny, n.Nz });

                    return false;
                }
            };
            ///
        }
    }
}

#endif // !GPRIMITIVES_H

