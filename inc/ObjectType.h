#pragma once

#ifndef OBJECTTYPE_H
#define OBJECTTYPE_H

namespace MedBox
{
	typedef enum
	{
		TRI_MESH_TYPE,
		QUAD_MESH_TYPE,
        IMAGE_TYPE,
        DICOM_IMG_COLL_TYPE
	} OBJECT_TYPE;
}
#endif //OBJECTTYPE_H