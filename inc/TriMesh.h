#pragma once

#ifndef MESH_H
#define MESH_H

#include "main.h"

#include "GPrimitives.h"
#include "ObjectType.h"

using Tri = MedBox::Modeling::Geometry::Triangle;
using Edg = MedBox::Modeling::Geometry::Edge;
using Vert = MedBox::Modeling::Geometry::Vertex;

namespace MedBox {

	namespace Modeling {

		class TriMesh
		{
		public:
			TriMesh(const list<Tri>&);
			TriMesh();
			~TriMesh();

		public:
			void Clear();
			void CalculateQuality();
			void Update();
			void GenerateTriangleNormals();

			//getters
			const vector<Tri>& GetTriangleList() const;
			const vector<Vert>& GetVertices() const;
			const vector<Edg>& GetEdges() const;
			const vector<vector<Uint32>>& GetVertexListToTriangleIndices() const;
			const vector<vector<Uint32>>& GetVertexListToVertexIndices() const;
			const vector<vector<Uint32>>& GetEdgeListToTriangleIndices() const;
			const vector<vector<Uint32>>& GetTriangleListToEdgeIndices() const;
			const vector<vector<Uint32>>& GetTriangleListToTriangleIndices() const;
			Uint32 GetFaceCount() const;
			Uint32 GetVertexCount() const;
			Uint32 GetEdgeCount() const;
			Float64 GetMeshQuality() const;
			OBJECT_TYPE GetType() const;

            bool VertexIsExist(const Vert& v) const;
            bool TriangleIsExist(const Tri& t) const;

		private:
			//Detect vertices, vertex to vertex indices and vertex to triangle indices
			void set_base_structures();

			void set_edges_and_triangle_to_edges_indices();
			void set_edge_to_triangle_indices();
			void set_triangle_to_triangle_indices();

		private:
			Float64 _quality;
			//Basic structures
			vector<Tri> _triangles;
			vector<Vert> _vertices;
			vector<Edg> _edges;

			vector<vector<Uint32>> _vertex_to_triangle_indices;
			vector<vector<Uint32>> _vertex_to_vertex_indices;
			vector<vector<Uint32>> _edge_to_triangle_indices;
			vector<vector<Uint32>> _triangle_to_edge_indices;
			vector<vector<Uint32>> _triangle_to_triangle_indices;

			OBJECT_TYPE _type = OBJECT_TYPE::TRI_MESH_TYPE;
		};
	}
}
#endif //MESH_H
