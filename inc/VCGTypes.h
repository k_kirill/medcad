#pragma once

#ifndef VCGTYPES_H
#define VCGTYPES_H

#include<vcg/complex/complex.h>

#include<vcg/complex/algorithms/clean.h>
#include<vcg/complex/algorithms/update/bounding.h>

// local optimization
#include <vcg/complex/algorithms/local_optimization.h>
#include <vcg/complex/algorithms/local_optimization/tri_edge_collapse_quadric.h>

using namespace vcg;
using namespace tri;

namespace VCGTypes
{
	//Forward declaration
	class VCGEdge;
	class VCGFace;
	class VCGVertex;

	struct VCGUsedTypes : public UsedTypes< Use<VCGVertex>	::AsVertexType,
											Use<VCGEdge>	::AsEdgeType,
											Use<VCGFace>	::AsFaceType> {};

	class VCGVertex : public Vertex< VCGUsedTypes, vertex::Coord3f,
												   vertex::Normal3f,
												   vertex::VFAdj,
												   vertex::BitFlags,
												   vertex::Mark,
												   vertex::Qualityf> 
	{
	public:
		math::Quadric<double> &Qd() { return q; }

	private:
		math::Quadric<double> q;
	};

	class VCGFace   : public Face< VCGUsedTypes, face::Mark,
												 face::VertexRef,
												 face::Normal3f,
												 face::BitFlags, 
												 face::VFAdj> {};

	class VCGEdge	: public Edge<VCGUsedTypes> {};
	class VCGMesh	: public tri::TriMesh< vector<VCGVertex>, vector<VCGFace>, vector<VCGEdge>  > {};

	//For decimate operation
	typedef BasicVertexPair<VCGVertex> VertexPair;

	class VCGTriEdgeCollapse : public tri::TriEdgeCollapseQuadric< VCGMesh, 
																   VertexPair, 
																   VCGTriEdgeCollapse, 
																   QInfoStandard<VCGVertex>  > {
	public:
		typedef  tri::TriEdgeCollapseQuadric< VCGMesh, VertexPair, VCGTriEdgeCollapse, QInfoStandard<VCGVertex>  > TECQ;
		typedef  VCGMesh::VertexType::EdgeType EdgeType;
		inline VCGTriEdgeCollapse(const VertexPair &p, int i, BaseParameterClass *pp) : TECQ(p, i, pp) {}
	};
}
#endif // !VCGTYPES_H

